var express = require('express'),
    path = require('path');
var MongoStore = require('connect-mongodb');
var MongoDb = require('mongodb').Db;
var Server = require('mongodb').Server;

var db = new MongoDb('sessionstore', new Server('localhost', 27017, {auto_reconnect: true, native_parser: false}));

module.exports = function (app) {
    var bodyParser = express.bodyParser();
    app.configure('production', function () {
        app.set('port', process.env.PORT || 8000);
        app.set('views', path.join(app.directory, '/dist'));
        app.engine('html', require('ejs').renderFile);
        app.set('view engine', 'html');
        app.use(express.favicon());
        app.use(express.logger('dev'));
        app.use(function(req,res,next){
            try{
                if(req.get('content-type').indexOf('multipart/form-data') === 0)return next();
                bodyParser(req,res,next);
            }catch(e){
                bodyParser(req,res,next);
            }
        });
        app.use(express.limit('1000mb'));
        app.use(express.methodOverride());
        app.use(express.cookieParser('your secret here'));
        app.use(express.session({ store: new MongoStore({db: db}), secret: 'super secret' }));
        app.use(app.router);
        
        app.use(express.static(path.join(app.directory, 'dist')));
    });
};
