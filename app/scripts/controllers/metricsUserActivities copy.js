'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsUserActivitiesCtrl', function ($scope,$http){

    $scope.profiles;
    $scope.stats;
    $scope.users;
    $scope.chartConfig = [];
    $scope.checkBoxModel = [];
    $scope.colorModel = [];

    $http.get('/stats').success(function(result){
        var i = result.length;
        while(i--){
            if(result[i].typeID != 11){ result.splice(i, 1); }
        }
        $scope.stats = result;

        $http.get('/users/sub').success(function(result){
            $scope.users = result;
        });

        $http.get('/organisation').success(function(result){
            $scope.profiles = result;

            for(var i = 0; i < result.length; i++){
                if($scope.checkBoxModel[i] === undefined){
                    $scope.checkBoxModel[i] = [];
                    $scope.colorModel[i] = [];
                }
                for(var k = 0; k < result[i].users.length; k++){
                    $scope.checkBoxModel[i][k] = false;
                    $scope.colorModel[i][k] = [];
                    var hasBeenShared = getLoggedinAmount(result[i].users[k]) > 0;
                    if(hasBeenShared){ $scope.toggleUser(result[i].users[k], i, k); }
                    $scope.checkBoxModel[i][k] = hasBeenShared;
                }
            }

        })
    });

    $scope.getName = function(id)
    {
        var user = _.findWhere($scope.users,{ _id : id });
        if(user){
            return user.firstname + ' ' + user.lastname;
        }else{
            return  '';
        }    
    }

    $scope.getChartConfig = function(index)
    {
        if($scope.chartConfig[index] === undefined){
            $scope.chartConfig[index] =
            {
                options: {
                    chart: {
                        type: 'line',
                        height: 400,
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '{series.name} {point.x:%e %b}: {point.y} logins'
                    }
                },
                title: { text: '' },
                xAxis:
                {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%e %b',
                        year: '%b'
                    },
                    title: { text: 'Date' }
                },
                yAxis: { title: { text: 'Amount of logins' }, min: 0 },
                series: [],
                loading: false
            }
        }
        return $scope.chartConfig[index];
    }

    $scope.toggleUser = function(userID, chartIndex, userIndex)
    {
        if($scope.chartConfig[chartIndex] === undefined){ $scope.getChartConfig(chartIndex); }
        
        var seriesArray = $scope.chartConfig[chartIndex].series;
        var objectIndex;
        for(var i = 0; i < seriesArray.length; i++){
            if(seriesArray[i].userID == userID){ objectIndex = i; }
        }

        if(objectIndex !== undefined){ // hide user info from chart
            seriesArray.splice(objectIndex, 1);
            $scope.colorModel[chartIndex][userIndex] = '';
        } else { // add user info from chart
            var color = getRandomColor();
            var newData =
            {
                name: $scope.getName(userID),
                userID: userID,
                color: color,
                data: getLoggedinData(userID)
            };
            $scope.colorModel[chartIndex][userIndex] = color;
            $scope.chartConfig[chartIndex].series.push(newData);
        }
        $scope.checkBoxModel[chartIndex][userIndex] = !$scope.checkBoxModel[chartIndex][userIndex];
    }

    function getRandomColor(){
        var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        if(color != '#FFFFFF'){ return color; }
        return getRandomColor();
    }

    function getLoggedinAmount(userID)
    {
        var count = 0;
        for(var i = 0; i < $scope.stats.length; i++){
            if($scope.stats[i].user_id == userID){ count++; }
        }
        return count;
    }

    function getLoggedinData(userID)
    {
        var data = [];
        var count = 0;

        var timeStamps = [];
        var timestamp = '';
        for(var i = 0; i < $scope.stats.length; i++){
            if($scope.stats[i].user_id == userID){
                timeStamps.push($scope.stats[i].timestamp);
            }
        }

        timeStamps.sort();
        
        var current = null;
        var cnt = 0;

        for(var i = 0; i < timeStamps.length; i++){
            if(timeStamps[i] != current){
                if(cnt > 0){
                    var date = new Date(current);
                    var item = [Date.UTC(date.getYear(), date.getMonth(), date.getDate()), cnt];
                    data.push(item);
                    // console.log(current + ' count = ' + cnt);
                }
                current = timeStamps[i];
                cnt = 1;
            } else { cnt++; }
        }
        if(cnt > 0){
            // console.log(current + ' count = ' + cnt);
            var date = new Date(current);
            var item = [Date.UTC(date.getYear(), date.getMonth(), date.getDate()), cnt];
            data.push(item);
        }

        return data;
    }

});