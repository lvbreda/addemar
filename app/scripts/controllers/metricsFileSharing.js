'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsFileSharingCtrl', function ($scope,$http){

    $scope.profiles;
    $scope.chartConfig = [];
    $scope.checkBoxModel = [];
    $scope.colorModel = [];

    $http.get('/stats')
    .success(function(result){

        var i = result.length;
        while(i--){
            if(result[i].typeID != 13){ result.splice(i, 1); }
        }
        $scope.stats = result;

        $http.get('/organisation')
        .success(function(result){
            $scope.profiles = result;

            for(var i = 0; i < result.length; i++){
                result[i].allFiles = getAllProfileFiles(result[i]);
            }

            for(var i = 0; i < result.length; i++){
                if($scope.checkBoxModel[i] === undefined){ $scope.checkBoxModel[i] = []; }
                for(var k = 0; k < result[i].allFiles.length; k++){
                    $scope.checkBoxModel[i][k] = false;
                    var hasBeenShared = getFileShareAmount(result[i].allFiles[k]._id, result[i]._id) > 0;
                    if(hasBeenShared){ $scope.toggleFile(result[i].allFiles[k], i, k); }
                    $scope.checkBoxModel[i][k] = hasBeenShared;
                }
            }

        })

    });

    function getAllProfileFiles(folderObject){
        var folderFiles = [];
        for(var i = 0; i < folderObject.files.length; i++){
            if(folderObject.files[i].dir_name !== undefined){
                folderFiles = folderFiles.concat(getAllProfileFiles(folderObject.files[i]));
            } else {
                folderFiles.push(folderObject.files[i]);
            }
        }
        return folderFiles;
    }

    $scope.getChartConfig = function(index)
    {
        if($scope.chartConfig[index] === undefined){
            $scope.chartConfig[index] =
            {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: ''
                },
                options: {
                    chart: {
                        type: 'line',
                    },
                    legend: {
                        enabled: false
                    },
                },
                series: [{
                    type: 'pie',
                    name: 'Shares',
                    data: []
                }]
            }
        }
        return $scope.chartConfig[index];
    }

    $scope.toggleFile = function(fileObject, chartIndex, fileIndex)
    {
        if($scope.chartConfig[chartIndex] === undefined){ $scope.chartConfig[chartIndex] = $scope.getChartConfig(chartIndex); }

        if($scope.checkBoxModel[chartIndex][fileIndex] == true){ // remove data
            var chartData = $scope.chartConfig[chartIndex].series[0].data;
            for(var i = 0; i < chartData.length; i++){
                var fileID = chartData[i][2];
                if(fileID == fileObject._id){
                    chartData.splice(i, 1);
                    break;
                }
            }
        } else { // add data
            var fileString = fileObject.filename;
            if(fileString.length > 20){ fileString = fileString.substring(0,20); fileString += '...'; }
            var newData = [fileString, getFileShareAmount(fileObject._id, $scope.profiles[chartIndex]._id), fileObject._id];
            $scope.chartConfig[chartIndex].series[0].data.push(newData);
        }

        $scope.checkBoxModel[chartIndex][fileIndex] = !$scope.checkBoxModel[chartIndex][fileIndex];
    }

    function getFileShareAmount(fileID, profileID)
    {
        var count = 0;
        for(var i = 0; i < $scope.stats.length; i++){
            if($scope.stats[i].fileID == fileID && $scope.stats[i].profileID == profileID){ count++; }
        }
        return count;
    }

    function getRandomColor()
    {
        var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        if(color != '#FFFFFF'){ return color; }
        return getRandomColor();
    }

});









