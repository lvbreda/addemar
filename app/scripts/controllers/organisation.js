'use strict';

angular.module('nodeFilehostApp')
  .controller('OrganisationCtrl', function ($scope,$routeParams,$http,$location,$fileUploader) {
    $scope.organisation = {};
    $scope.organisation.users = {};
    $scope.retrievedUsers = [];
    $scope.uniqueProfileNameError = false;
    
    if($routeParams.id){
    	$http.get('organisation/' + $routeParams.id).success(function(result){
    		$scope.organisation = result;
            var i = result.users.length;
            while (i--) {
                if(result.users[i] == ''){ result.users.splice(i, 1); }
            }
    	});
    }
    $http.get('users/sub').success(function(result){
    	$scope.retrievedUsers = result;
    })

    $http.get('/organisation').success(function(result){
        $scope.organisations = result;
        for(var i in $scope.organisations){
            // $scope.organisations[i].files = folderSort($scope.organisations[i].files);
        }
        
    })
    $scope.click_upload = function(){
         
            $('#file_upload').click();
        }

   
    $scope.uploader = $fileUploader.create({
            url: '/upload',
            autoUpload : true
        });

   
      $scope.uploader.bind('complete', function (event, xhr, item, response) {
            $scope.organisation.logo = response._id;
            
            $scope.$emit('file_added',response);
        });

    $scope.save = function(){
        console.log($scope.organisation);
        for(var i in $scope.organisations){
            if($scope.organisations[i].name == $scope.organisation.name){
                $scope.uniqueProfileNameError = true;
                return;
            }
        }
    	if($routeParams.id){
    		$http.put('/organisation/' + $routeParams.id,$scope.organisation).success(function(result){
    			history.back();
    		})
    	}else{
    		$http.post('organisation',$scope.organisation).success(function(result){
    			history.back();
    		})
    	}
    }
    
  });