'use strict';

angular.module('nodeFilehostApp')
  .controller('RegisterCtrl', function ($scope,User,$location,$http) {

    $scope.users;
    $http.get('/users').success(function(result){
      $scope.users = result;
    });

   	$scope.register = function(username,password){
   		$http.post("/users/create",{
   			username : username,
   			password : password
   		}).success(function(result){
   			User.login(username,password,function(result){
   					$location.path('/home');
   			})
   		})
   	}
  });
