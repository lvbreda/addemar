'use strict';

angular.module('nodeFilehostApp')
  .controller('usersAddCtrl', function ($scope,$http,$routeParams) {
    $scope.users;
    $scope.selectedUser = {};
    $scope.error;

    $http.get('/users/sub').success(function(result){
    	$scope.users = result;
        if($routeParams.id){
            for(var i in $scope.users){
                if($scope.users[i]._id == $routeParams.id){
                    $scope.selectedUser = $scope.users[i];
                }
            }
        }
    });

   /** if($routeParams.id){
        var users = $scope.users;
        for(user in users){

            // if(users[user]._id)
        }
        $scope.selectedUser
    }**/

    $scope.setSelectedUser = function(user){
    	$scope.selectedUser = user;
    }

    $scope.save = function(){

    	if($scope.selectedUser._id){
    		$http.put('/users/sub',$scope.selectedUser).success(function(result){
    			$scope.selectedUser = {};
    			$http.get('/users/sub')
                .success(function(result){
			    	$scope.users = result;
                    history.back();
			    })
    		})
            .error(function(error){
                history.back();
            });
    	}else{
    		$http.post('/users/sub',$scope.selectedUser)
            .success(function(result){
    			$scope.selectedUser = {};
    			$http.get('/users/sub')
                .success(function(result){
			    	$scope.users = result;
                    history.back();
			    });
    		})
            .error(function(error){
                    $scope.error = true;;
            });
    	}

    }
  });
