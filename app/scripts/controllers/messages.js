'use strict';

angular.module('nodeFilehostApp')
  .controller('messagesCtrl', function ($scope,$http, $location) {

    $scope.sendtoAllProfiles = true;
    $scope.messageSending = false;
    $scope.messageSend = false;

    $scope.users;
    $scope.selectedUser = {};
    $scope.msg;
    $scope.messages;
    $scope.errorMessage;
    $scope.organisation = [];
    $scope.organisation.users;
    $scope.organisations;

    $http.get('/users/sub').success(function(result){

    	$scope.users = result;
        console.log('----');
        console.log('all users (' + result.length + ')');
        console.log(result);
    });

    $http.get('/organisation').success(function(result){
        $scope.organisations = result;
    })

    $http.get('/notifications').success(function(result){
        for(var i = 0; i < result.length; i++){
            var d = new Date(result[i].date);
            var dateFormat = getMonthName(d.getMonth()) + " " + d.getDate() + ", " + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes();
            result[i].date = dateFormat;
        }
        $scope.messages = result;
    });
    $scope.getCount = function(org,users){
        var count = 0;
        for(var i in org.users){
            if(_.find(users,function(it){return it._id == org.users[i]})){
                count = count +1;
            }
        }   
        return count;
    }
    $scope.sendMsg  = function(){

        $scope.messageSending = true;

        if(!$scope.sendtoAllProfiles && $scope.organisation.users === undefined){
            $scope.errorMessage = 'Your message doesn\'t have any receivers.';
            return;
        }

        var users = [];
        if($scope.sendtoAllProfiles == "1"){
            users = $scope.users;
        } else {
            for(var i = 0; i < $scope.users.length; i++){
                for(var j = 0; j < $scope.organisation.users.length; j++){
                    if($scope.users[i]._id == $scope.organisation.users[j]){
                        users.push($scope.users[i]);
                    }
                }
            }
        }

        if(users.length <= 0){
            $scope.errorMessage = 'Your message doesn\'t have any receivers.';
            return;
        }

        $http.post('/notifications/send', {
            message : $scope.msg,
            user : users,
            date : new Date()
        }).success(function(result){
            $scope.msg = '';
            
            $http.get('/notifications').success(function(result){
                for(var i = 0; i < result.length; i++){
                    var d = new Date(result[i].date);
                    var dateFormat = getMonthName(d.getMonth()) + " " + d.getDate() + ", " + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes();
                    result[i].date = dateFormat;
                }
                $scope.messages = result;
                $scope.messageSending = false;
                $scope.messageSend = true;
            });
        }).error(function(error){
            console.log(error);
        });
    }
    
    function getMonthName(monthID){
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        return month[monthID];
    }
  });
