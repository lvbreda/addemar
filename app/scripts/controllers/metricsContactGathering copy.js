'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsContactGatheringCtrl', function ($scope,$http){
    
    // profiles
    $scope.profiles;
    $http.get('/organisation')
    .success(function(result){
        $scope.profiles = result;
    })

    // stats
    $http.get('/stats').success(function(result){
        var logins = _.groupBy(_.filter(result,function(it){ return it.typeID == 11 }),function(item){
            return item.fileID;
        })
        console.log(logins);
    });

    // users
    $scope.users;
    $http.get('/users/sub').success(function(result){
        $scope.users = result;
    })

    $scope.getName = function(id)
    {
        console.log('------');
        console.log(id);
        var user = _.findWhere($scope.users,{ _id : id });
        if(user){
            return user.firstname + ' ' + user.lastname;
        }else{
            return  '';
        }    
    }

    // charts
    $scope.chartConfig = [];
    $scope.checkBoxModel = [];
    $scope.colorModel = [];
    $scope.getChartConfig = function(index)
    {
        if($scope.chartConfig[index] === undefined){
            // general config
            $scope.checkBoxModel[index] = [];
            $scope.colorModel[index] = [];
            $scope.chartConfig[index] =
            {
                options: {
                    chart: {
                        type: 'column',
                        height: 400,
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '<b>{point.name}:</b> {point.y} contacts'
                    }
                },
                title: { text: '' },
                
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '12px',
                            // fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },

                yAxis: { title: { text: 'Contacts gathered' } },

                series: [{
                    name: 'Contact gathered',
                    data:
                    [
                        ['User 1', 5],
                        ['User 2', 34],
                        ['User 3', 2],
                        ['User 4', 18],
                        ['User 5', 19],
                        ['User 6', 22]
                    ],
                    dataLabels:
                    {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '12px',
                            // fontFamily: 'Verdana, sans-serif',
                            // textShadow: '0 0 3px black'
                        }
                    }
                }]
            }

            /*
            // show first 3 users
            if($scope.profiles[index] !== undefined){
                for(var i = 0; i < $scope.profiles[index].users.length; i++){
                    if(i < 8){
                        $scope.toggleUser($scope.profiles[index].users[i], index, i);
                        $scope.checkBoxModel[index][i] = true;
                    }
                }       
            }
            */
        }
        return $scope.chartConfig[index];
    }

    $scope.toggleUser = function(userID, chartIndex, userIndex)
    {
        if($scope.chartConfig[chartIndex] === undefined){ return; }
        
        var seriesArray = $scope.chartConfig[chartIndex].series;
        var objectIndex;
        for(var i = 0; i < seriesArray.length; i++){
            if(seriesArray[i].userID == userID){ objectIndex = i; }
        }

        if(objectIndex !== undefined){ // hide user info from chart
            seriesArray.splice(objectIndex, 1);
            $scope.colorModel[chartIndex][userIndex] = '';
        } else { // add user info from chart
            var dummData = $scope.dummyRandom[Math.floor((Math.random() * 4) + 0)];
            var color = getRandomColor();
            var newData =
            {
                name: $scope.getName(userID),
                userID: userID,
                color: color,//$scope.colors[$scope.currentColor],
                data: dummData
            };
            $scope.colorModel[chartIndex][userIndex] = color;
            $scope.chartConfig[chartIndex].series.push(newData);
        }
    }

    function getRandomColor(){
        var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        if(color != '#FFFFFF'){ return color; }
        return getRandomColor();
    }








    $scope.dummyRandom =
    [

        [
            ['test', 2],
            [Date.UTC(2014, 5, 2), 8],
            [Date.UTC(2014, 5, 3), 23],
            [Date.UTC(2014, 5, 5), 1],
            [Date.UTC(2014, 5, 6), 8],
            [Date.UTC(2014, 5, 7), 3],
            [Date.UTC(2014, 5, 9), 8],
            [Date.UTC(2014, 5, 10), 9],
            [Date.UTC(2014, 5, 11), 2],
            [Date.UTC(2014, 5, 12), 0],
            [Date.UTC(2014, 5, 13), 12]
        ],

        [
            [Date.UTC(2014, 5, 1), 2],
            [Date.UTC(2014, 5, 2), 8],
            [Date.UTC(2014, 5, 3), 2],
            [Date.UTC(2014, 5, 5), 19],
            [Date.UTC(2014, 5, 6), 25],
            [Date.UTC(2014, 5, 7), 37],
            [Date.UTC(2014, 5, 9), 3],
            [Date.UTC(2014, 5, 10), 0],
            [Date.UTC(2014, 5, 11), 0],
            [Date.UTC(2014, 5, 12), 4],
            [Date.UTC(2014, 5, 13), 7]
        ],

        [
            [Date.UTC(2014, 5, 1), 8],
            [Date.UTC(2014, 5, 2), 4],
            [Date.UTC(2014, 5, 3), 9],
            [Date.UTC(2014, 5, 5), 0],
            [Date.UTC(2014, 5, 6), 2],
            [Date.UTC(2014, 5, 7), 2],
            [Date.UTC(2014, 5, 9), 2],
            [Date.UTC(2014, 5, 10), 7],
            [Date.UTC(2014, 5, 11), 8],
            [Date.UTC(2014, 5, 12), 4],
            [Date.UTC(2014, 5, 13), 1]
        ],

        [
            [Date.UTC(2014, 5, 1), 0],
            [Date.UTC(2014, 5, 2), 0],
            [Date.UTC(2014, 5, 3), 0],
            [Date.UTC(2014, 5, 5), 2],
            [Date.UTC(2014, 5, 6), 3],
            [Date.UTC(2014, 5, 7), 4],
            [Date.UTC(2014, 5, 9), 8],
            [Date.UTC(2014, 5, 10), 9],
            [Date.UTC(2014, 5, 11), 12],
            [Date.UTC(2014, 5, 12), 36],
            [Date.UTC(2014, 5, 13), 8]
        ],

        [
            [Date.UTC(2014, 5, 1), 8],
            [Date.UTC(2014, 5, 2), 4],
            [Date.UTC(2014, 5, 6), 12],
            [Date.UTC(2014, 5, 7), 21],
            [Date.UTC(2014, 5, 9), 18],
            [Date.UTC(2014, 5, 10), 0],
            [Date.UTC(2014, 5, 11), 4],
            [Date.UTC(2014, 5, 12), 9],
            [Date.UTC(2014, 5, 13), 23]
        ]

    ];

});






