'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsFileSharingCtrl', function ($scope,$http){
    
    // profiles
    $scope.profiles;
    $http.get('/organisation')
    .success(function(result){
        $scope.profiles = result;
    })

    // stats
    $http.get('/stats').success(function(result){
        var logins = _.groupBy(_.filter(result,function(it){ return it.typeID == 11 }),function(item){
            return item.fileID;
        })
        console.log(logins);
    });

    // users
    $scope.users;
    $http.get('/users/sub').success(function(result){
        $scope.users = result;
    })

    $scope.getName = function(id)
    {
        var user = _.findWhere($scope.users,{ _id : id });
        if(user){
            return user.firstname + ' ' + user.lastname;
        }else{
            return  '';
        }    
    }

    // charts
    $scope.chartConfig = [];
    $scope.checkBoxModel = [];
    $scope.colorModel = [];
    $scope.getChartConfig = function(index)
    {
        if($scope.chartConfig[index] === undefined){
            // general config
            $scope.checkBoxModel[index] = [];
            $scope.colorModel[index] = [];
            $scope.chartConfig[index] =
            {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: ''
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.series}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Shares',
                    data: [
                        ['Dummy file 1<br/>5 Shares', 5],
                        ['Dummy file 2<br/>8 Shares', 8],
                        ['Dummy file 3<br/>2 Shares', 2],
                        ['Dummy file 4<br/>12 Shares', 12],
                        ['Dummy file 5<br/>7 Shares', 7]
                    ]
                }]
            }

            // show first 3 users
            if($scope.profiles[index] !== undefined){
                for(var i = 0; i < $scope.profiles[index].users.length; i++){
                    if(i < 8){
                        $scope.toggleUser($scope.profiles[index].users[i], index, i);
                        $scope.checkBoxModel[index][i] = true;
                    }
                }       
            }
        }
        return $scope.chartConfig[index];
    }

    $scope.toggleUser = function(userID, chartIndex, userIndex)
    {
        return;
        if($scope.chartConfig[chartIndex] === undefined){ return; }
        
        var seriesArray = $scope.chartConfig[chartIndex].series;
        var objectIndex;
        for(var i = 0; i < seriesArray.length; i++){
            if(seriesArray[i].userID == userID){ objectIndex = i; }
        }

        if(objectIndex !== undefined){ // hide user info from chart
            seriesArray.splice(objectIndex, 1);
            $scope.colorModel[chartIndex][userIndex] = '';
        } else { // add user info from chart
            var dummData = $scope.dummyRandom[Math.floor((Math.random() * 4) + 0)];
            var color = getRandomColor();
            var newData =
            {
                name: $scope.getName(userID),
                userID: userID,
                color: color,//$scope.colors[$scope.currentColor],
                data: dummData
            };
            $scope.colorModel[chartIndex][userIndex] = color;
            $scope.chartConfig[chartIndex].series.push(newData);
        }
    }

    function getRandomColor(){
        var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        if(color != '#FFFFFF'){ return color; }
        return getRandomColor();
    }

});






