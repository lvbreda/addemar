'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsContactGatheringCtrl', function ($scope,$http){

    $scope.users;
    $scope.stats;
    $scope.checkBoxModel = [];
    $scope.colorModel = [];
    $scope.selectedItems = [];

    $http.get('/stats')
    .success(function(result){

        var i = result.length;
        while(i--){
            if(result[i].typeID != 12){ result.splice(i, 1); }
        }
        $scope.stats = result;

        $http.get('/users/sub').success(function(result){
            $scope.users = result;
            for(var i = 0; i < result.length; i++){
                var hasSharedContact = getContactsShareAmount(result[i]._id) > 0;
                if(hasSharedContact){ $scope.toggleUser(i, result[i]); }
                $scope.checkBoxModel[i] = hasSharedContact;
            }
        });

    });

    $scope.chartConfig =
    {
        options: {
            chart: {
                type: 'column',
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '<b>{point.name}:</b> {point.y} contacts'
            }
        },
        title: { text: '' },
        
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                align: 'right',
                style: {
                    fontSize: '12px',
                    // fontFamily: 'Verdana, sans-serif'
                }
            }
        },

        yAxis: { title: { text: 'Contacts gathered' } },

        series: [{
            name: 'Contact gathered',
            data:
            [],
            dataLabels:
            {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                x: 4,
                y: 10,
                style: {
                    fontSize: '12px',
                    // fontFamily: 'Verdana, sans-serif',
                    // textShadow: '0 0 3px black'
                }
            }
        }]
    }

    $scope.toggleUser = function(index, userObject)
    {
        if($scope.checkBoxModel[index] == true){ // remove data
            var chartData = $scope.chartConfig.series[0].data;
            for(var i = 0; i < chartData.length; i++){
                var userID = chartData[i][2];
                if(userID == userObject._id){
                    chartData.splice(i, 1);
                    break;
                }
            }
        } else { // add data
            $scope.checkBoxModel[index] = true;
            var userName = userObject.firstname + ' ' + userObject.lastname;
            var newData = [userName, getContactsShareAmount(userObject._id), userObject._id];
            $scope.chartConfig.series[0].data.push(newData);
        }
        $scope.checkBoxModel[index] = !$scope.checkBoxModel[index];
    }

    function getContactsShareAmount(userID)
    {
        var count = 0;
        for(var i = 0; i < $scope.stats.length; i++){
            if($scope.stats[i].user_id == userID){ count++; }
        }
        return count;
    }

    function getRandomColor(){
        var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        if(color != '#FFFFFF'){ return color; }
        return getRandomColor();
    }

});