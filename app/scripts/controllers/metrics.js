'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsCtrl', function ($scope,$http){

  	// tab system
  	$scope.profileUsage = false;
	$scope.contentSharing = false;
	$scope.contactGathering = false;
	$scope.userLoggins = true;

	$scope.hideAllTabs = function()
	{
		$scope.profileUsage = false;
		$scope.contentSharing = false;
		$scope.contactGathering = false;
		$scope.userLoggins = false;
	}

	// profiles
  	$scope.profiles;
  	$http.get('/organisation')
  	.success(function(result){
		$scope.profiles = result;
    })

    // stats
    $http.get('/stats').success(function(result){
		var logins = _.groupBy(_.filter(result,function(it){ return it.typeID == 11 }),function(item){
			return item.fileID;
		})
		console.log(logins);
	});

    // users
    $scope.users;
    $http.get('/users/sub').success(function(result){
    	$scope.users = result;
    })

    $scope.getName = function(id)
    {
        var user = _.findWhere($scope.users,{ _id : id });
        if(user){
            return user.firstname + ' ' + user.lastname;
        }else{
            return  '';
        }    
    }

    // charts
    $scope.chartConfig = [];
    $scope.userLoginCheckBoxModel = [];
    $scope.colorModel = [];
    $scope.getChartConfig = function(index)
    {
    	if($scope.chartConfig[index] === undefined){
    		// general config
    		$scope.userLoginCheckBoxModel[index] = [];
    		$scope.colorModel[index] = [];
    		$scope.chartConfig[index] =
    		{
		        options: {
		            chart: {
		            	type: 'line',
		            	height: 370,
		            },
		            legend: {
		            	enabled: false
		        	},
					tooltip: {
		            	headerFormat: '<b>{series.name}</b><br>',
		            	pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
		        	}
		        },
		        title: { text: '' },
		        xAxis:
		        {
		            type: 'datetime',
		            dateTimeLabelFormats: {
		                month: '%e. %b',
		                year: '%b'
		            },
		            title: { text: 'Date' }
		        },
		        yAxis: { title: { text: 'Amount of logins' }, min: 0 },
		        series: [],
		        loading: false
		    }

		    // show first 3 users
		    if($scope.profiles[index] !== undefined){
		    	for(var i = 0; i < $scope.profiles[index].users.length; i++){
					if(i < 8){
						$scope.toggleUser($scope.profiles[index].users[i], index, i);
						$scope.userLoginCheckBoxModel[index][i] = true;
					}
				}    	
		    }
    	}
    	return $scope.chartConfig[index];
    }

    $scope.toggleUser = function(userID, chartIndex, userIndex)
    {
    	if($scope.chartConfig[chartIndex] === undefined){ return; }
    	
    	var seriesArray = $scope.chartConfig[chartIndex].series;
    	var objectIndex;
    	for(var i = 0; i < seriesArray.length; i++){
    		if(seriesArray[i].userID == userID){ objectIndex = i; }
    	}

    	if(objectIndex !== undefined){ // hide user info from chart
    		seriesArray.splice(objectIndex, 1);
    		$scope.colorModel[chartIndex][userIndex] = '';
    	} else { // add user info from chart
    		var dummData = $scope.dummyRandom[Math.floor((Math.random() * 4) + 0)];
    		var color = getRandomColor();
    		var newData =
	    	{
	    		name: $scope.getName(userID),
	    		userID: userID,
	    		color: color,//$scope.colors[$scope.currentColor],
	        	data: dummData
	        };
	        $scope.colorModel[chartIndex][userIndex] = color;
	        $scope.chartConfig[chartIndex].series.push(newData);
    	}
    }

    function getRandomColor(){
    	var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
    	if(color != '#FFFFFF'){ return color; }
    	return getRandomColor();
    }








    $scope.dummyRandom =
    [

    	[
    		[Date.UTC(2014, 5, 1), 2],
            [Date.UTC(2014, 5, 2), 8],
            [Date.UTC(2014, 5, 3), 23],
            [Date.UTC(2014, 5, 5), 1],
            [Date.UTC(2014, 5, 6), 8],
            [Date.UTC(2014, 5, 7), 3],
            [Date.UTC(2014, 5, 9), 8],
            [Date.UTC(2014, 5, 10), 9],
            [Date.UTC(2014, 5, 11), 2],
            [Date.UTC(2014, 5, 12), 0],
            [Date.UTC(2014, 5, 13), 12]
    	],

    	[
    		[Date.UTC(2014, 5, 1), 2],
            [Date.UTC(2014, 5, 2), 8],
            [Date.UTC(2014, 5, 3), 2],
            [Date.UTC(2014, 5, 5), 19],
            [Date.UTC(2014, 5, 6), 25],
            [Date.UTC(2014, 5, 7), 37],
            [Date.UTC(2014, 5, 9), 3],
            [Date.UTC(2014, 5, 10), 0],
            [Date.UTC(2014, 5, 11), 0],
            [Date.UTC(2014, 5, 12), 4],
            [Date.UTC(2014, 5, 13), 7]
    	],

    	[
    		[Date.UTC(2014, 5, 1), 8],
            [Date.UTC(2014, 5, 2), 4],
            [Date.UTC(2014, 5, 3), 9],
            [Date.UTC(2014, 5, 5), 0],
            [Date.UTC(2014, 5, 6), 2],
            [Date.UTC(2014, 5, 7), 2],
            [Date.UTC(2014, 5, 9), 2],
            [Date.UTC(2014, 5, 10), 7],
            [Date.UTC(2014, 5, 11), 8],
            [Date.UTC(2014, 5, 12), 4],
            [Date.UTC(2014, 5, 13), 1]
    	],

    	[
    		[Date.UTC(2014, 5, 1), 0],
            [Date.UTC(2014, 5, 2), 0],
            [Date.UTC(2014, 5, 3), 0],
            [Date.UTC(2014, 5, 5), 2],
            [Date.UTC(2014, 5, 6), 3],
            [Date.UTC(2014, 5, 7), 4],
            [Date.UTC(2014, 5, 9), 8],
            [Date.UTC(2014, 5, 10), 9],
            [Date.UTC(2014, 5, 11), 12],
            [Date.UTC(2014, 5, 12), 36],
            [Date.UTC(2014, 5, 13), 8]
    	],

    	[
    		[Date.UTC(2014, 5, 1), 8],
            [Date.UTC(2014, 5, 2), 4],
            [Date.UTC(2014, 5, 6), 12],
            [Date.UTC(2014, 5, 7), 21],
            [Date.UTC(2014, 5, 9), 18],
            [Date.UTC(2014, 5, 10), 0],
            [Date.UTC(2014, 5, 11), 4],
            [Date.UTC(2014, 5, 12), 9],
            [Date.UTC(2014, 5, 13), 23]
    	]

    ];

});






