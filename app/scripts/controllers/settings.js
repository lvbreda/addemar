'use strict';

angular.module('nodeFilehostApp')
  .controller('SettingsCtrl', function ($scope,$routeParams,$http,$location,$fileUploader,$timeout) {

    $scope.privacy = false;
    $scope.complaint = false;
    $scope.beta = false;

    if($routeParams.page){
      switch($routeParams.page){
        case 'privacy-policy': $scope.privacy = true; break;
        case 'complaint-procedure': $scope.complaint = true; break;
        case 'beta-agreement': $scope.beta = true; break;
      }
    }

});