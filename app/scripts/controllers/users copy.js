'use strict';

angular.module('nodeFilehostApp')
  .controller('UsersCtrl', function ($scope,$http) {
    $scope.users;
    $scope.selectedUser = {};

    $scope.test = "test variable";


    $http.get('/users/sub').success(function(result){
    	$scope.users = result;
    });

    $scope.setSelectedUser = function(user){
    	$scope.selectedUser = user;
    }

    $scope.save = function(){
    	if($scope.selectedUser._id){
    		$http.put('/users/sub',$scope.selectedUser).success(function(result){
    			$scope.selectedUser = {};
    			$http.get('/users/sub').success(function(result){
			    	$scope.users = result;
			    })
    		})
    	}else{
    		$http.post('/users/sub',$scope.selectedUser).success(function(result){
    			$scope.selectedUser = {};
    			$http.get('/users/sub').success(function(result){
			    	$scope.users = result;
			    })
    		})
    	}
    }
  });
