'use strict';

angular.module('nodeFilehostApp')
  .controller('ProfilesCtrl', function ($scope, $fileUploader,$http, $timeout){

    $scope.files_dom;
    $scope.all_files;
    $scope.selectedOrganisation;
    $scope.organisations;
    $scope.selectedDir = '';
    $scope.addDir = false;
    $scope.dirName = '';
    $scope.urlName = '';
    $scope.addUrlToggle = false;

    $scope.libraryStatus = 'Loading files';
    $scope.profileStatus = 'Select a profile';
    $scope.selectedLibraryFiles = [];
    $scope.selectedProfileFiles = [];
    $scope.hasIcon = false;
    $scope.hasBackground = false;
    $scope.selectedImg = false;

    $http.get('/files').success(function(result){
        var fileString = (result.length == 1) ? 'file' : 'files';
        $scope.libraryStatus = 'Loaded ' + result.length + ' ' + fileString;
        $scope.libraryFiles = result;
    });

    $http.get('/files').success(function(result){
        var temp = {};
        $scope.all_files = result;
        for(var i in $scope.all_files){
            temp[$scope.all_files[i]._id] = $scope.all_files[i];
        }
        $scope.all_files = temp;
    });

    $http.get('/organisation').success(function(result){
        $scope.organisations = result;
        for(var i in $scope.organisations){
            $scope.organisations[i].files = folderSort($scope.organisations[i].files);
        }
        
    })
    var folderSort = function(items){
        for(var i in items){
            if(items[i].dir_name){
                items[i].files = folderSort(items[i].files);
            }
        }
        return _.sortBy(items,function(item){
            return item.order;
        })
    }

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var getCurrentProfileFiles = function(){
        var collectedFiles = [];
        var profileFiles = $scope.selectedOrganisation.files;

        if($scope.selectedDir != ''){
            for(var o in profileFiles){
                if(profileFiles[o].dir_name == $scope.selectedDir){
                    var dirFiles = profileFiles[o].files;
                    for(var d in dirFiles){
                        // console.log(dirFiles[d]);
                    }
                }
            }
        } else {
            for(var p in profileFiles){
                var selectedProfileFiles = $scope.selectedProfileFiles;

                // root folder
                for(var s in selectedProfileFiles){
                    if(profileFiles[p]._id == selectedProfileFiles[s]._id){
                        // console.log(profileFiles[p]);
                        collectedFiles.push(profileFiles[p]);
                    }
                }
            }
        }
    }
    $scope.getImages = function(dir,files){
        var folder;
        if(_.isEmpty($scope.selectedDir)){
            return {};
        }else{
            folder = getFolder($scope.selectedOrganisation.files,$scope.selectedDir);
            if(folder.background){ $scope.hasBackground = true; }
            if(folder.icon){ $scope.hasIcon = true; }
            return {
                icon : folder.icon,
                background : folder.background
            }
        }
    }
    $scope.getFolder =  function(dir,org){
        $scope.hasIcon = false;
        $scope.hasBackground = false;
        var folder;
        if(_.isEmpty($scope.selectedDir) && $scope.selectedOrganisation){
            return $scope.selectedOrganisation.files;
        }else if($scope.selectedOrganisation){
            // console.log($scope.selectedDir,$scope.selectedOrganisation);
            folder = getFolder($scope.selectedOrganisation.files,$scope.selectedDir);

            return folder.files;
        }
    }
    $scope.recurseDelete  = function(org,id){
        org.files = _.reject(org.files,function(item){
            return item._id == id;
        })
        for(var i in org.files){
            if(org.files[i].dir_name){
                $scope.recurseDelete(org.files[i].dir_name);
            }
        }
    }
    $scope.deleteSelectedLibraryFiles = function(){

        var fileString = ($scope.selectedLibraryFiles.length == 1) ? 'file' : 'files';
        if(!confirm("Are you sure you want to delete " + $scope.selectedLibraryFiles.length + " " + fileString + "? This action cannot be undone.")){
            return;
        }

        for(var i in $scope.selectedLibraryFiles){
            $http.delete('/file/' + $scope.selectedLibraryFiles[i]._id ).success(function(res,err){
               for(var o in $scope.organisations){
                $scope.recurseDelete($scope.organisations[o],$scope.selectedLibraryFiles[i]._id);
                $http.put('/organisation/'+ $scope.organisations[o]._id, $scope.organisations[o]).success(function(result){
                   
                });
               }
            })
        }

        var fileString = ($scope.selectedLibraryFiles.length == 1) ? 'file' : 'files';
        $scope.libraryStatus = 'Deleted ' + $scope.selectedLibraryFiles.length + ' ' + fileString;
        $scope.selectedLibraryFiles = [];

        $timeout(function(){
         $http.get('/files').success(function(result){
                // $scope.libraryFiles = result;
            })
        },2000)
    }
    $scope.deleteSelectedProfileFiles = function(){

        if(!confirm("Are you sure you want to remove these files? The files wil not be deleted from the library.")){ return; }

        var folder;
        if(_.isEmpty($scope.selectedDir)){
            folder = $scope.selectedOrganisation.files;
        }else{
            folder = getFolder($scope.selectedOrganisation.files,$scope.selectedDir).files;
        }

        // console.log(folder,$scope.selectedProfileFiles);
        var ids = _.map($scope.selectedProfileFiles,function(item){
            return  item._id;
        });
        
        console.log(ids);
        console.log(folder);

        // LANDER VRAAG splice = index van ID veranderd, daardoor werkt delete niet meer?
        folder = _.reject(folder,function(item){
            return ids.indexOf(item._id)> -1;
        })
        console.log(folder);
        console.log($scope.selectedOrganisation.files);

        setRecursive($scope.selectedOrganisation,folder,$scope.selectedDir)

        console.log($scope.selectedOrganisation);
        // left off
        // console.log(ids);
       /** for(var i in folder){
            console.log('- folder item');
            if(ids.indexOf(folder[i]._id) > -1){
                console.log('--- folder item is id');
                folder.splice(i,1);
            }
        }**/
      
        $http.put('/organisation/'+ $scope.selectedOrganisation._id, $scope.selectedOrganisation).success(function(result){
            $http.get('/organisation').success(function(result){
                // $scope.organisations = result;
                var fileString = ($scope.selectedProfileFiles.length == 1) ? 'file' : 'files';
                $scope.profileStatus = 'Removed ' + $scope.selectedProfileFiles.length + ' ' + fileString;
                $scope.selectedProfileFiles = [];
            })
        });
        // $scope.selectedProfileFiles = [];
        //getCurrentProfileFiles();
    }
    $scope.setFileAsIcon = function(file){
        for(var i in $scope.selectedProfileFiles){
            $scope.selectedProfileFiles[i].icon = file._id;
        }
        $http.put('/organisation/'+ $scope.selectedOrganisation._id, $scope.selectedOrganisation).success(function(result){
            $http.get('/organisation').success(function(result){
                // $scope.organisations = result;
            })
        });

    }
    $scope.updateSelectedLibraryFiles = function($event, selectedFile){
        var checkbox = $event.target;
        if(checkbox.checked) $scope.selectedLibraryFiles.push(selectedFile);
        if(!checkbox.checked) $scope.selectedLibraryFiles.splice(selectedFile, 1);
        var fileString = ($scope.selectedLibraryFiles.length == 1) ? 'file' : 'files';
        var extension = selectedFile.filename.split('.').pop();
        $scope.selectedImg = (extension == 'jpg' || extension == 'png' || extension == 'jpeg' || extension == 'gif');
        $scope.libraryStatus = 'Selected ' + $scope.selectedLibraryFiles.length + ' ' + fileString;
    }

    $scope.updateSelectedProfileFiles = function($event, selectedFile){
        var checkbox = $event.target;
        if(checkbox.checked) $scope.selectedProfileFiles.push(selectedFile);
        if(!checkbox.checked) $scope.selectedProfileFiles.splice(selectedFile, 1);
        var fileString = ($scope.selectedProfileFiles.length == 1) ? 'file' : 'files';
        $scope.profileStatus = 'Selected ' + $scope.selectedProfileFiles.length + ' ' + fileString;
    }
    $scope.deleteSelectedOrganisation = function(){

        if(!confirm("Are you sure you want to delete this profile? This action cannot be undone.")){
            return;
        }

        $http.delete('/organisation/'+  $scope.selectedOrganisation._id).success(function(result){
            $scope.selectedOrganisation = undefined;

            $http.get('/organisation').success(function(result){
                $scope.organisations = result;
            })

        })
    }
    $scope.addFilesToProfile = function(){
        if($scope.selectedLibraryFiles.length <= 0){ alert('Please select files to copy first.'); return; }
        if(!$scope.selectedOrganisation){ alert('Please select a profile first.'); return; }

        var folder = getFolder($scope.selectedOrganisation.files, $scope.selectedDir);
        if(!folder){ folder = $scope.selectedOrganisation; }

        var libraryFiles = $scope.selectedLibraryFiles;

        for(var l in libraryFiles){
            libraryFiles[l].order = _.values(folder.files).length +1 ;
            folder.files.push(libraryFiles[l]);
        }
        
        $http.put('/organisation/'+ $scope.selectedOrganisation._id, $scope.selectedOrganisation).success(function(result){
            $http.get('/organisation').success(function(result){
                // $scope.organisations = result;
            })
        });
    }

    $scope.setFileAsBackground = function(fileID){
        if($scope.selectedLibraryFiles.length <= 0){ alert('Please select files to copy first.'); return; }
        if(!$scope.selectedOrganisation){ alert('Please select a profile first.'); return; }
        var folder;
        if(!$scope.selectedDir){
            folder = $scope.selectedOrganisation;
        }else{
            folder = getFolder($scope.selectedOrganisation.files, $scope.selectedDir);
        }
        folder.background = fileID._id;
        $http.put('/organisation/'+ $scope.selectedOrganisation._id, $scope.selectedOrganisation).success(function(result){
            $http.get('/organisation').success(function(result){
                // $scope.organisations = result;
            })
        });
    }

    var getFolder = function(files, folderName){
        var folder;
        for(var i in files){
            // console.log(files[i].dir_name);
            if(files[i].dir_name){

                if(files[i].dir_name == folderName){
                   // console.log(folderName,files[i].dir_name);
                   folder = files[i];
                } else {
                   var temp = getFolder(files[i].files, folderName);
                   if(temp){
                    folder = temp;
                   }
                   //folder =  getFolder(files[i].files, folderName);
                }
            }
        }
        return folder;
    }
    var setRecursive = function(org,files,folderName,ne){
        var folder;
        if(!folderName){
            org.files = files;
        }else{
            for(var i in org.files){
                // console.log(files[i].dir_name);
                if(org.files[i].dir_name){

                    if(org.files[i].dir_name == folderName){
                       org.files[i].files = files;
                       return;
                    } else {
                       setRecursive(org.files[i],files,folderName, ne);
                       
                       //folder =  getFolder(files[i].files, folderName);
                    }
                }
            }
        }
        
        return ;
    }
    $scope.goOrganisation = function(organisation){
        $scope.selectedProfileFiles = [];
        $scope.selectedOrganisation = organisation;
        $scope.selectedDir = '';
        if(!$scope.selectedOrganisation.files){
            $scope.selectedOrganisation.files = {};
        }
        $scope.files_dom = $scope.selectedOrganisation.files;
    }

    $scope.getFiles = function(files, organisations){
        var p = [];
        for(var i in files){
            if(!files[i].dir_name){
                p.push($scope.all_files[i])
            }else{
                p.push(files[i]);
            }
        }
        return p;
    }
    
    $scope.goFolder = function(folder){
        $scope.selectedDir = folder.dir_name;
        $scope.selectedProfileFiles = [];
    }

    $scope.createDir = function(){
        var folder = getFolder($scope.selectedOrganisation.files, $scope.selectedDir);
        if(!folder){ folder = $scope.selectedOrganisation; }
        folder.files.push( {
            dir_name : $scope.dirName,
            _id : makeid(),
            files : []
        })
        // console.log(folder);
        $http.put('/organisation/'+ $scope.selectedOrganisation._id,$scope.selectedOrganisation).success(function(result){
            $http.get('/organisation').success(function(result){
                // $scope.organisations = result;
            })
        })
        $scope.dirName = '';
    }

    $scope.click_upload = function(){
        $('#file_upload').click();
    }

    $scope.addUrl = function(){
        $http.post('/upload/link',{
            url : $scope.urlName
        }).success(function(result){

            var folder = getFolder($scope.selectedOrganisation.files, $scope.selectedDir);
            if(!folder){ folder = $scope.selectedOrganisation; }
            folder.files.push(result);
            
            // LANDER VRAGEN kan ik hier ID van nieuwe file krijgen (om toe te voegen aan profile)
            //$scope.$emit('file_added', result);

           $http.put('/organisation/'+ $scope.selectedOrganisation._id,$scope.selectedOrganisation).success(function(result){
                $http.get('/organisation').success(function(result){
                    // $scope.organisations = result;
                    $scope.addUrlToggle=false;
                })
            })
        });
        reloadLibrary();
        $scope.urlName = '';
    }

    var makeid = function(){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    var reloadLibrary = function(){
        $http.get('/files').success(function(result){
            $scope.libraryFiles = result;
        });
    }

    var uploader = $scope.uploader = $fileUploader.create({
        url: '/upload',
        autoUpload : true
    });

    uploader.bind('progressall', function (event, progress) {
        $scope.libraryStatus = 'Uploading files (' + progress + '%)';
        // console.info('Progress: ' + progress, item);
    });
    
    uploader.bind('complete', function (event, xhr, item, response) {

        $scope.libraryStatus = 'Uploaded files';
        if(!response.error){
            $scope.libraryFiles.push(response);
        }
        

        // console.log($scope.files_dom, $scope.selectedOrganisation);
        //reloadLibrary();
    });
    uploader.bind('completeall', function (event, xhr, item, response) {

        $scope.libraryStatus = 'Uploaded files';
       // $scope.libraryFiles.push(response);

        // console.log($scope.files_dom, $scope.selectedOrganisation);
        reloadLibrary();
    });

    $scope.dropSuccessHandler = function(e,p,i){
        //console.log(e,p,i);
    }
    $scope.onDrop  = function(e,data,folder){
        var folder = getFolder($scope.selectedOrganisation.files, $scope.selectedDir);
        if(!folder){ folder = $scope.selectedOrganisation; }
            data.order = _.values(folder.files).length + 1;
            folder.files.push(data);
            //var order = _.values(folder.files).length;
           // folder.files[data._id].order = order;
        
        // console.log(folder,$scope.selectedOrganisation);
        $http.put('/organisation/'+ $scope.selectedOrganisation._id, $scope.selectedOrganisation).success(function(result){
            $http.get('/organisation').success(function(result){
                // $scope.organisations = result;
            })
        });
    }
  });