'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsCtrl', function ($scope,$http){


  	$scope.organisations;
  	$scope.stats;
  	$scope.statsMail ;
	$scope.profileUsage = true;
	$scope.contentSharing = false;
	$scope.contactGathering = false;
	$scope.userLoggins = false;
	$scope.emails = [];
	$scope.contacts;

	$scope.chartData = [];

	// PROFILE USAGE CHARTS
	$scope.chartType = 'pie';

	$scope.chartConfig = {
    	labels: true,
    	tooltips: true,
    	title : "",
    	legend : {
        	display: true,
        	position: 'left'
    	},
	}

	$scope.getDataModel = function(index){
		if($scope.chartData[index] === undefined){ $scope.chartData[index] = []; }
		return $scope.chartData[index];
	}
	
	$scope.hideAllTabs = function(){
		$scope.profileUsage = false;
		$scope.contentSharing = false;
		$scope.contactGathering = false;
		$scope.userLoggins = false;
	}

	$scope.getCount;

	$http.get('/organisation').success(function(result){
		$scope.organisations = result;
    })

    $http.get('/contacts').success(function(result){
        $scope.contacts = result;
    })
	
	$http.get('/stats').success(function(result){
		var group = _.groupBy(_.filter(result,function(it){return it.type == 'mobile'}),function(item){
			return item.fileID;
		})
		var groupMail = _.groupBy(_.filter(result,function(it){return it.type == 'mail'}),function(item){
			return item.fileID;
		})
		$scope.stats = group;
		$scope.statsMail = groupMail;

		var out = _.map(_.filter(result,function(it){return it.type == 'mail'}),function(item){
			return item.email;
		})
		$scope.emails = out;
		// console.log($scope.statsMail)


		// create profile usage
		$http.get('/organisation').success(function(result){
			var organisations = result;

			// console.log('----- GET STATS -----')
			for(var i = 0; i < organisations.length; i++){
				var organisation = organisations[i];
				var files = organisation.files;
				var series = [];
				var data = [];
				for(var k = 0; k < files.length; k++){
					series.push(files[k]);
					if(group[files[k]._id] !== undefined){

						/*
						console.log(files[k]._id);
						console.log(group[files[k]._id].length);
						console.log('--');
						*/

						data.push({ x : files[k].filename, y: [group[files[k]._id].length], tooltip: files[k].filename });
					}
					// console.log(group);
					// [files[i]._id].length
				}
				$scope.chartData[i] = {
				    series: series,
				    data : data   
				};
			}
	    });

		return;

		var files = $scope.organisations[index].files;
		var series = [];
		for(var i = 0; i < files.length; i++){
			series.push(files[i]);
			console.log($scope.stats);
			// [files[i]._id].length
		}

	});

	$scope.getCountMail = function(id){
		return;
		if(!$scope.statsMail){
			return "";
		}
		// console.log(id,$scope.statsMail)
		return $scope.statsMail[id].length;
	}
	
  });