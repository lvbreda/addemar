'use strict';

angular.module('nodeFilehostApp')
  .controller('EditfileCtrl', function ($scope,$routeParams,$http) {
    	$scope.file;

    	$http.get('/file/' + $routeParams.id  +'/info').success(function(result){
 			$scope.file = result;
    	})


    	$scope.save = function(){
    		$http.put('/file/'+ $routeParams.id,$scope.file.metadata).success(function(res){
    			history.back();
    		})
    	}
  });
