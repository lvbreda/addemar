'use strict';

angular.module('nodeFilehostApp')
  .controller('LoginCtrl', function ($scope,User,$location,$http) {
   	$scope.login = function(username,password){
   		User.login(username,password,function(){
   			$location.path('/home');
   		})
   	}
  });
