'use strict';

angular.module('nodeFilehostApp')
 	.controller('contactsCtrl', function ($scope,User,$location,$http){

		$scope.contacts;
		$scope.users;

		$http.get('/users/sub').success(function(result){
    		$scope.users = result;

    		$http.get('/orgcontacts').success(function(result){
				$scope.contacts = result;

				for(var i = 0; i < result.length; i++){
					var date = result[i].added;
					var year = result[i].added.substr(0, 4);
					var month = result[i].added.substr(5, 2);
					var day = result[i].added.substr(8, 2);
					result[i].added = day + ' ' + month + ' ' + year;


					for(var k = 0; k < $scope.users.length; k++){
						if(result[i].user_id == $scope.users[k]._id){
							result[i].username = $scope.users[k].username;
						}
					}
				}
			});

    	});

    	$scope.exportSCV = function(req, res){

    		var fileContent = 'firstname, lastname, email, company, created by, created at';
			for(var i = 0; i < $scope.contacts.length; i++){
				fileContent += '\n';
				fileContent += $scope.contacts[i].lastname + ', ';
				fileContent += $scope.contacts[i].lastname + ', ';
				fileContent += $scope.contacts[i].email + ', ';
				fileContent += $scope.contacts[i].company + ', ';
				fileContent += $scope.contacts[i].username + ', ';
				fileContent += $scope.contacts[i].added;
			}

    		var encodedUri = encodeURI(fileContent);
	      	var link = document.createElement("a");
	      	link.setAttribute("href", "data:text/csv;charset=utf-8," + encodedUri);
	      	link.setAttribute("download", "export.csv");
	      	link.click();

    	}

});