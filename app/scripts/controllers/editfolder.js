'use strict';

var find_folder =  function(tree,check){
	for(var i in tree.files){
		if(tree.files[i].files){
			if(tree.files[i]._id == check){
				return tree.files[i];
			}else{
				return find_folder(tree.files[i],check);
			}
		}
	}
}


angular.module('nodeFilehostApp')
  .controller('EditfolderCtrl', function ($scope,$routeParams,$fileUploader,$http) {
  	 $scope.organisation;
  	 $scope.folder;
     $scope.uploading = 'back'
     if($routeParams.id){
    	$http.get('/organisation/' + $routeParams.id).success(function(result){
    		$scope.organisation = result;
    		console.log($scope.organisation);
    		$scope.folder = find_folder($scope.organisation,$routeParams.folderid);
    		console.log()
    	});
     }



     $scope.save = function(){
     	$http.put('/organisation',$scope.organisation).success(function(result){
               history.back();
         })
     }
     $scope.click_upload = function(){
            $scope.uploading = 'back';
            $('#file_upload').click();
        }

    $scope.click_upload2 = function(){
            $scope.uploading = 'back2'
            $('#file_upload2').click();
    }
     var uploader = $scope.uploader = $fileUploader.create({
            url: '/upload',
            autoUpload : true
        });

   
      uploader.bind('complete', function (event, xhr, item, response) {
        console.log($scope.folder);
            if($scope.uploading == 'back'){
              $scope.folder.background = response._id;
            }else{
              $scope.folder.icon = response._id;
            }
           // $scope.$emit('file_added',response);
        });
      


     
  });
