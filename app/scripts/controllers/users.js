'use strict';

angular.module('nodeFilehostApp')
  .controller('UsersCtrl', function ($scope,$http,$location) {

    $scope.users;
    $scope.organisations;
    $scope.selectedUser = {};

    $http.get('/users/sub').success(function(result){
    	$scope.users = result;
    });

    $http.get('/organisation').success(function(result){
        $scope.organisations = result;
    })

    $scope.setSelectedUser = function(user){
    	$scope.selectedUser = user;
    }
    $scope.deleteUser = function(id){

        if(!confirm("Are you sure you want to delete this user? This action cannot be undone.")){ return; }


        for(var i in $scope.organisations){
            for(var k in $scope.organisations[i].users){
                if($scope.organisations[i].users[k] == id){
                    $scope.organisations[i].users.splice(k, 1);
                }
            }
            $http.put('/organisation/'+ $scope.organisations[i]._id, $scope.organisations[i]).success(function(result){
            }).error(function(error){
                console.log(error);
            });
        }
        
        $http.delete('/users/sub/' + id).success(function(outcome){
            $http.get('/users/sub').success(function(result){
                for(var i in $scope.organisations){
                    $scope.deleteUserFromOrganisation($scope.organisations[i],id);
                }
                $scope.users = result;
            });
        });
    }
    $scope.getName = function(id,users){
        var user = _.findWhere(users,{_id : id})
        if(user){
            return user.username;
        }else{
            return  '';
        }
        
    }
    $scope.exists = function(id,users){
        var user = _.findWhere(users,{_id : id})
        if(user){
           return true; 
        }else{
            return false;
        }
        
    }
    $scope.deleteUserFromOrganisation = function(org, id){
        for(var i in org.users){
            if(org.users[i] == id){
                org.users.splice(i,1);
            }
        }
        $http.put('/organisation/'+ org._id, org).success(function(result){
        }).error(function(error){});
    }
    $scope.save = function(){
    	if($scope.selectedUser._id){
    		$http.put('/users/sub',$scope.selectedUser).success(function(result){
    			$scope.selectedUser = {};
    			$http.get('/users/sub').success(function(result){
			    	$scope.users = result;
                    history.back();
			    })
    		})
    	}else{
    		$http.post('/users/sub',$scope.selectedUser).success(function(result){
    			$scope.selectedUser = {};
    			$http.get('/users/sub').success(function(result){
			    	$scope.users = result;
                    history.back();
			    })
    		})
    	}
    }
    $scope.dropSuccessHandler = function(e,p,i){
        //console.log(e,p,i);
    }
    $scope.onDrop  = function(e,data,org){
       if(org.users.indexOf(data._id)==-1){
        org.users.push(data._id);

        $http.put('/organisation/' + org._id,org).success(function(result){
                //history.back();
        })
       }
    }
  });
