'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsUserActivitiesCtrl', function ($scope,$http){

    $scope.profiles;
    $scope.stats;
    $scope.users;
    $scope.chartConfig;
    $scope.checkBoxModel = [];
    $scope.colorModel = [];

    $http.get('/stats').success(function(result){
        var i = result.length;
        while(i--){
            if(result[i].typeID != 11){ result.splice(i, 1); }
        }
        $scope.stats = result;

        $http.get('/users/sub').success(function(result){
            $scope.users = result;
            for(var i = 0; i < result.length; i++){
                var hasData = getLoggedinData(result[i]._id).length > 0;
                if($scope.checkBoxModel[i] === undefined){ $scope.checkBoxModel[i] = false; }
                if(hasData){ $scope.toggleUser(i, result[i]); }
            }
        });
    });

    $scope.getName = function(id)
    {
        var user = _.findWhere($scope.users,{ _id : id });
        if(user){
            return user.firstname + ' ' + user.lastname;
        }else{
            return  '';
        }    
    }

    $scope.chartConfig =
    {
        options: {
            chart: {
                type: 'line',
                height: 400,
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '{series.name} {point.x:%e %b}: {point.y} logins'
            }
        },
        title: { text: '' },
        xAxis:
        {
            type: 'datetime',
            dateTimeLabelFormats: {
                month: '%e %b',
                year: '%b'
            },
            title: { text: 'Date' }
        },
        yAxis: { title: { text: 'Amount of logins' }, min: 0 },
        series: [],
        loading: false
    }

    $scope.toggleUser = function(userIndex, userObject)
    {

        if($scope.checkBoxModel[userIndex]){ // remove data
            var chartData = $scope.chartConfig.series;
            for(var i = 0; i < chartData.length; i++){
                var userID = chartData[i].userID;
                if(userID == userObject._id){
                    chartData.splice(i, 1);
                    $scope.colorModel[userIndex] = '';
                    break;
                }
            }
        } else { // add data
            var color = getRandomColor();
            var newData =
            {
                name: userObject.firstname + ' ' + userObject.lastname,
                userID: userObject._id,
                color: color,
                data: getLoggedinData(userObject._id)
            };
            $scope.colorModel[userIndex] = color;
            $scope.chartConfig.series.push(newData);
        }
        $scope.checkBoxModel[userIndex] = !$scope.checkBoxModel[userIndex];

        console.log('achter = ');
        console.log($scope.checkBoxModel[userIndex]);
    }

    function getRandomColor(){
        var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        if(color != '#FFFFFF'){ return color; }
        return getRandomColor();
    }

    function getLoggedinAmount(userID)
    {
        var count = 0;
        for(var i = 0; i < $scope.stats.length; i++){
            if($scope.stats[i].user_id == userID){ count++; }
        }
        return count;
    }

    function getLoggedinData(userID)
    {
        var data = [];
        var count = 0;

        var timeStamps = [];
        var timestamp = '';
        for(var i = 0; i < $scope.stats.length; i++){
            if($scope.stats[i].user_id == userID){
                timeStamps.push($scope.stats[i].timestamp);
            }
        }

        timeStamps.sort();
        
        var current = null;
        var cnt = 0;

        for(var i = 0; i < timeStamps.length; i++){
            if(timeStamps[i] != current){
                if(cnt > 0){
                    var date = new Date(current);
                    var item = [Date.UTC(date.getYear(), date.getMonth(), date.getDate()), cnt];
                    data.push(item);
                    // console.log(current + ' count = ' + cnt);
                }
                current = timeStamps[i];
                cnt = 1;
            } else { cnt++; }
        }
        if(cnt > 0){
            // console.log(current + ' count = ' + cnt);
            var date = new Date(current);
            var item = [Date.UTC(date.getYear(), date.getMonth(), date.getDate()), cnt];
            data.push(item);
        }

        return data;
    }

});