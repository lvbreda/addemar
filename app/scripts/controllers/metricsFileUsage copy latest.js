'use strict';

angular.module('nodeFilehostApp')
  .controller('metricsFileUsageCtrl', function ($scope,$http){

  	$scope.profiles;
  	$http.get('/organisation')
  	.success(function(result){
		$scope.profiles = result;
    });


    $scope.profiles;
    $scope.chartConfig = [];
    $scope.checkBoxModel = [];
    $scope.colorModel = [];

    $http.get('/stats')
    .success(function(result){

        var i = result.length;
        while(i--){
            if(result[i].typeID != 13){ result.splice(i, 1); }
        }
        $scope.stats = result;

        $http.get('/organisation')
        .success(function(result){
            $scope.profiles = result;

            for(var i = 0; i < result.length; i++){
                result[i].allFiles = getAllProfileFiles(result[i]);
            }

            for(var i = 0; i < result.length; i++){
                if($scope.checkBoxModel[i] === undefined){ $scope.checkBoxModel[i] = []; }
                for(var k = 0; k < result[i].allFiles.length; k++){
                    $scope.checkBoxModel[i][k] = false;
                    var hasBeenShared = getFileShareAmount(result[i].allFiles[k]._id, result[i]._id) > 0;
                    if(hasBeenShared){ $scope.toggleFile(result[i].allFiles[k], i, k); }
                    $scope.checkBoxModel[i][k] = hasBeenShared;
                }
            }

        })

    });


    // files
    $scope.getName = function(id)
    {
        var file = _.findWhere($scope.files,{ _id : id });
        if(file){ return file.filename.substring(0, 20); }
        return  '';
    }
    
    // stats
    $scope.fileOpens;
    $http.get('/stats').success(function(result){
        // get files
        $scope.files;
        $http.get('/files')
        .success(function(result){
            $scope.files = result;
            for(var i = 0; i < 8; i++){ // toggle first 8 files
                if(result[i]){
                    $scope.toggleFile(i, result[i]._id);
                }
            }
        });

        // get file opens
        var fileOpens = _.groupBy(_.filter(result,function(it){return it.typeID == 10; }),function(item){
            return item.fileID;
        });
        $scope.fileOpens = fileOpens;
	});

    // charts
    $scope.colorModel = [];
    $scope.checkBoxModel = [];
    $scope.selectedItems = [];
    $scope.chartConfig =
    {
        options: {
            chart: {
                type: 'bar',
                height: 550,
            },
            legend: { enabled: true },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                valueSuffix: ' millions'
            }
        },

        title: { text: '' },
        scrollbar: { enabled: true },

        xAxis:
        {
            categories: [],
            title: {
                text: null
            }
        },

        yAxis:
        {
            min: 0,
            title: {
                text: 'File opens',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },

        plotOptions:
        {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },

        legend:
        {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
            shadow: true
        },

        series: [],

        loading: false
    }

    $scope.toggleFile = function(index, fileID)
    {
        $scope.checkBoxModel[index] = true;
        $scope.colorModel[index] = getRandomColor();
        $scope.selectedItems[index] = fileID;

        // xAxis categories
        var fileNames = [];
        for(var i = 0; i < $scope.selectedItems.length; i++){
            fileNames[i] = $scope.getName($scope.selectedItems[i]);
        }
        $scope.chartConfig.xAxis.categories = fileNames;

        // series
        var series = [];
        for(var i = 0; i < $scope.profiles.length; i++){
            // files
            var data = [];
            for(var k = 0; k < $scope.selectedItems.length; k++){
                data[k] = getOpenedAmount($scope.selectedItems[k]);
            }

            // profiles
            var serie =
            {
                name: $scope.profiles[i].name,
                data: data
            }
            series[i] = serie;
        }

        $scope.chartConfig.series = series;
    }

    function getOpenedAmount(fileID){
        if($scope.fileOpens[fileID]){
            return $scope.fileOpens[fileID].length;
        }
    }

    function getRandomColor(){
    	var color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
    	if(color != '#FFFFFF'){ return color; }
    	return getRandomColor();
    }

});






