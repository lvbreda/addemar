'use strict';

angular.module('nodeFilehostApp')
  .controller('UploadCtrl', function ($scope, $fileUploader,$http) {

        $scope.files_dom;
        $scope.all_files;
        $scope.selectedOrganisation;
        $scope.organisations;
        $scope.selectedDir = '';
        $scope.addDir = false;

        $http.get('/files').success(function(result){
            var temp = {};
            $scope.all_files = result;
            for(var i in $scope.all_files){
                temp[$scope.all_files[i]._id] = $scope.all_files[i];
            }
            $scope.all_files = temp;
        });

        $http.get('/organisation').success(function(result){
            $scope.organisations = result;
        })

        $scope.$on('file_added',function(msg,file){
            console.log('Added',file);
            $scope.files_dom[file._id]  = file;
            $http.put('/organisation',$scope.selectedOrganisation).success(function(result){
                $http.get('/organisation').success(function(result){
                    $scope.organisations = result;
                })
            })
        })
        $scope.click_upload = function(){
            $('#file_upload').click();
        }
        $scope.goOrganisation = function(organisation){
            $scope.selectedOrganisation = organisation;
            $scope.selectedDir = '';
            if(!$scope.selectedOrganisation.files){
                $scope.selectedOrganisation.files = {};
            }
            $scope.files_dom = $scope.selectedOrganisation.files;
            
        }
        $scope.goFolder = function(folder){
            $scope.selectedDir = folder.dir_name;
            for(var i in $scope.files_dom){
                if($scope.files_dom[i].dir_name && $scope.files_dom[i].dir_name == folder.dir_name){
                    $scope.files_dom = $scope.files_dom[i].files;
                }
            }
        }
        $scope.createDir = function(name){
            $scope.files_dom['afold' + makeid()] = {
                dir_name : name,
                _id : makeid(),
                files : {

                }
            }
            $http.put('/organisation',$scope.selectedOrganisation).success(function(result){
                $http.get('/organisation').success(function(result){
                    $scope.organisations = result;
                })
            })
        }
        $scope.getFiles = function(files,organisations){
            var p = [];
            for(var i in files){
                if(!files[i].dir_name){
                    p.push($scope.all_files[i])
                }else{
                    p.push(files[i]);
                }
            }
            return p;
        }
        $scope.addUrl = function(url){
            $http.post('/upload/link',{
                url : url
            }).success(function(result){
                $scope.$emit('file_added',result);
                $http.get('/organisation').success(function(result){
                    $scope.organisations = result;
                })
            })
        }





    	var uploader = $scope.uploader = $fileUploader.create({
            scope:$scope,
            url: '/upload',
            autoUpload : true,
            removeAfterUpload : true
        });

        var makeid = function()
        {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for( var i=0; i < 5; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }







        // ADDING FILTERS

        // Images only
        uploader.filters.push(function(item /*{File|HTMLInputElement}*/) {
            var type = uploader.isHTML5 ? item.type : '/' + item.value.slice(item.value.lastIndexOf('.') + 1);
            type = '|' + type.toLowerCase().slice(type.lastIndexOf('/') + 1) + '|';
            return true;
        });


        // REGISTER HANDLERS

        uploader.bind('afteraddingfile', function (event, item) {
            console.info('After adding a file', item);
        });

        uploader.bind('afteraddingall', function (event, items) {
            console.info('After adding all files', items);
        });

        uploader.bind('beforeupload', function (event, item) {
            console.info('Before upload', item);
        });

        uploader.bind('progress', function (event, item, progress) {
            console.info('Progress: ' + progress, item);
        });

        uploader.bind('success', function (event, xhr, item, response) {
            console.info('Success', xhr, item, response);
        });

        uploader.bind('cancel', function (event, xhr, item) {
            console.info('Cancel', xhr, item);
        });

        uploader.bind('error', function (event, xhr, item, response) {
            console.info('Error', xhr, item, response);
        });

        uploader.bind('complete', function (event, xhr, item, response) {
            $scope.$emit('file_added',response);
            console.log($scope.files_dom,$scope.selectedOrganisation);
           
        });

        uploader.bind('progressall', function (event, progress) {
            console.info('Total progress: ' + progress);
        });

        uploader.bind('completeall', function (event, items) {
           /** $http.get('/files').success(function(result){
                $scope.all_files = result;
            });**/
        });
  });
