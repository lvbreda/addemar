'use strict';

angular.module('nodeFilehostApp', ['ngRoute','ngSanitize','angularFileUpload','ui.select2','ngDragDrop','ui', 'highcharts-ng'])
  .config(function ($routeProvider) {
    $routeProvider

      .when('/home', {
        templateUrl: 'views/home.html'
      })

      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })

      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl'
      })

      .when('/profiles', {
        templateUrl: 'views/profiles.html',
        controller: 'ProfilesCtrl'
      })

      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl'
      })
      .when('/users/add',{
        templateUrl: 'views/usersAdd.html',
        controller: 'usersAddCtrl'
      })
      .when('/users/add/:id', {
        templateUrl: 'views/usersAdd.html',
        controller: 'usersAddCtrl'
      })
      .when('/settings/:page', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })



      .when('/metrics', {
        templateUrl: 'views/metricsUserActivities.html',
        controller: 'metricsUserActivitiesCtrl'
      })

      .when('/metrics/file-usage', {
        templateUrl: 'views/metricsFileUsage.html',
        controller: 'metricsFileUsageCtrl'
      })

      .when('/metrics/file-sharing', {
        templateUrl: 'views/metricsFileSharing.html',
        controller: 'metricsFileSharingCtrl'
      })

      .when('/metrics/contact-gathering', {
        templateUrl: 'views/metricsContactGathering.html',
        controller: 'metricsContactGatheringCtrl'
      })


      .when('/messages', {
        templateUrl: 'views/messages.html',
        controller: 'messagesCtrl'
      })

      .when('/contacts', {
        templateUrl: 'views/contacts.html',
        controller: 'contactsCtrl'
      })




      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/acl', {
        templateUrl: 'views/acl.html',
        controller: 'AclCtrl'
      })
      .when('/upload', {
        templateUrl: 'views/upload.html',
        controller: 'UploadCtrl'
      })
      .when('/organisation', {
        templateUrl: 'views/organisation.html',
        controller: 'OrganisationCtrl'
      })
      .when('/organisation/:id', {
        templateUrl: 'views/organisation.html',
        controller: 'OrganisationCtrl'
      })
      .when('/organisations', {
        templateUrl: 'views/organisations.html',
        controller: 'OrganisationsCtrl'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl'
      })
      .when('/editfile/:id', {
        templateUrl: 'views/editfile.html',
        controller: 'EditfileCtrl'
      })
      .when('/editfolder/:id/:folderid', {
        templateUrl: 'views/editfolder.html',
        controller: 'EditfolderCtrl'
      })
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })
      .otherwise({
        redirectTo: '/login'
      });
  }).run(function($rootScope,$location){

    $rootScope.showTutorial = false;

    $rootScope.isLogin = function()
    {
      if($location.path().indexOf('login')>-1){
        return true;
      }else{
        return false;
      }
    }

    $rootScope.goBack  = function()
    {
      history.back();
    }

    $rootScope.isActive = function(viewLocation)
    {
      return viewLocation === $location.path();
    };

  });
