'use strict';

angular.module('nodeFilehostApp')
  .service('User', function User($rootScope,$http) {
    var self = this;
    self.currentUser  = {};
    self.login = function(username,password,success){
    	$http.post('users/login',{
    		username : username,
    		password : password
    	}).success(function(res){
    		self.setCurrentUser(res);
    		success(self.getCurrentUser());
    	}).error(function(res){
    		$rootScope.$emit('error',res);
    	})
    }
    self.getCurrentUser = function(){
    	return self.currentUser;
    }
    self.setCurrentUser = function(user){
    	self.currentUser = user;
    }
    return self;
  });
