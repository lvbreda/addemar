'use strict';

describe('Controller: EditfolderCtrl', function () {

  // load the controller's module
  beforeEach(module('nodeFilehostApp'));

  var EditfolderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditfolderCtrl = $controller('EditfolderCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
