'use strict';

describe('Controller: OrganisationsCtrl', function () {

  // load the controller's module
  beforeEach(module('nodeFilehostApp'));

  var OrganisationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrganisationsCtrl = $controller('OrganisationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
