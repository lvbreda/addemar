'use strict';

describe('Controller: EditfileCtrl', function () {

  // load the controller's module
  beforeEach(module('nodeFilehostApp'));

  var EditfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditfileCtrl = $controller('EditfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
