/**
{
	name,
	description,
	users : [],
	files : {
		id : file,
		folder : {
			name ,
			files : {
	
			}
		}
	}
}


**/

var mongodb = require('mongodb'); 
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var Grid = require('gridfs-stream');

var recurse = function(files,func){
	for(var i in files){
		if(i != 'dir_name'){
			if(files[i].dir_name){
				files[i].files = recurse(files[i].files,func);
			}else{
				files[i] = func(i);
			}
		}	
	}
	return files;
}
var update_organisation = function(req,res){
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
		console.log(err);
		if(!db){
	    	return update_organisation(req,res);
	    }
	    //if(err) throw err;
	    var collection = db.collection('organisation');
	    delete req.body._id;
	    req.body.modified = (new Date()).getTime();
	    if(req.body.full){
	    	req.body.files = recurse(req.body.files,function(i){return {}})
	    	delete req.body.full;
	    }
	    collection.update({_id : ObjectID(req.params.id)},{$set:req.body},function(err,docs){
	    	if(err){
	    		res.json(500,{
	    			"code" : 500,
	    			"message" : err
	    		})
	    	}else{
	    		res.json(200,{});
	    	}
	    	db.close();
	    })
	 })
}

var get_organisations = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(!db){
	    	return get_organisations(req,res);
	    }
	    var collection = db.collection('organisation');
	   	collection.find({owner_id : req.session.user._id}).toArray(function(err,organisations){
	   		res.json(200,organisations);
	   		db.close();
	   	});
	 })
}
var get_organisation = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(err) throw err;
	    var collection = db.collection('organisation');
	   	collection.find({owner_id : req.session.user._id, _id : ObjectID(req.params.id) }).toArray(function(err,organisations){
	   		res.json(200,organisations[0]);
	   		db.close();
	   	});
	 })
}

var rec = function(files){
	var out = {};
	for(var i in files){
		if(files[i].dir_name){
			out[files[i]._id]  = {
				dir_name : files[i].dir_name,
				_id : files[i]._id,
				icon : files[i].icon,
				background: files[i].background
			};
			out[files[i]._id].files = rec(files[i].files);
		}else{
			out[files[i]._id] = files[i];
		}
	}
	return out;
}
var get_organisation_detailed = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(err) throw err;
	    var collection = db.collection('organisation');
	   	collection.find({_id : ObjectID(req.params.id) }).toArray(function(err,organisations){
	   		var organisation = organisations[0];
	   		var p = [];
	   		
	   		//organisation.files = rec(organisation.files);
	   		res.json(200,organisation);
	   		return;
	   		recurse(organisation.files,function(i){
	   			p.push(i._id);
	   			return i._id;
	   		})
	   		
	   			var gfs = Grid(db, mongodb);
	   			var temp = {};
		    	gfs.files.find({ _id : {$in :p} }).toArray(function (err, files) {
				   /**for(var i in files){
				   	temp[files[i]._id] = files[i];
				   }**/
				   /**recurse(organisation.files,function(o){
				   	return temp[o];
				   })**/
				   res.json(200,organisation);
				   db.close();
				})
	   		
	   	});
	 })
}


var delete_organisation = function(req,res){
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(err) throw err;
	    var collection = db.collection('organisation');
	   	collection.remove({_id  : ObjectID(req.params.id)},function(err,result){
	   		res.json(200,{});
	   	});
	   })
}
var get_organisations_sub = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(err) throw err;
	    var collection = db.collection('organisation');
	   	collection.find({users  : req.session.user._id}).toArray(function(err,organisations){

	   		
	   		/**for(var i in organisations){
	   			organisations[i].files = rec(organisations[i].files);
	   		}**/
	   		res.json(200,organisations);
	   		db.close();
	   	});
	 })

}
exports.update_organisation = update_organisation;
exports.get_organisations = get_organisations;
exports.get_organisation = get_organisation;
exports.get_organisation_detailed = get_organisation_detailed;
exports.delete_organisation = delete_organisation;
exports.get_organisations_sub = get_organisations_sub;
