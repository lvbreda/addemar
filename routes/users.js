var acl = require('acl');
var ObjectID = require('mongodb').ObjectID;
var mongodb = require('mongodb');
var bcrypt = require('bcryptjs');
var randomstring = require("randomstring");
mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
  acl = new acl.mongodbBackend(db, 'acl_');
}); 


var get_current_user =function(req,res){
	res.json(200,req.session.user);
}
var set_logo = function(req,res){
	var logo = req.body.logo;
	console.log("Hi logo",logo,req.session.user._id);
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		if(!db){
			return set_logo(req,res);
		}
		db.collection('users').update({
			_id : ObjectID(req.session.user._id),
		},{
			$set:{
				'logo' : logo
			}
		},function(err){
			if(err){
				console.log(err);
				res.json(500,err);
			}else{
				res.json(200,{
					message : "Logo updated"
				})
			}
			db.close();
		})
		
	});
}
var get_logo = function(req,res){
	var owner = ObjectID(req.session.user.owner_id);
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("users").find({
			_id : owner	
		}).toArray(function(err,result){
			res.json(200,{
				logo : result[0].logo
			});
			db.close();
		})
		
	});
}
var register_device = function(req,res){
	var device_id = req.body.device;
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection('users').update({
			_id : ObjectID(req.session.user._id),
		},{
			$push : {
				devices : device_id
			}
		},function(err){
			if(err){
				console.log(err);
				res.json(500,err);
			}else{
				res.json(200,{
					message : "Device registered"
				})
			}
			db.close();
		})
		
	});

}
var create_user = function(req,res){
	var username = req.body.username;
	var password = bcrypt.hashSync(req.body.password, 8);
	console.log(req.body);
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("users").insert({
			username  : username,
			password : password
		},function(err){
			if(err){
				console.log(err);
				res.json(500,err);
			}else{
				res.json(200,{
					username : username
				})
			}
			db.close();
		})
	});
}

var create_sub_user = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	var username = req.body.username;
	var password = bcrypt.hashSync(req.body.password, 8);
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("users").findOne({username:username},function(err,doc){
			if(doc){
				res.json(500,{
					msg : "Duplicate username"
				})
			}else{
				db.collection("users").insert({
					username  : username,
					password : password,
					firstname : req.body.firstname,
					lastname : req.body.lastname,
					email : req.body.email,
					type : "sub",
					owner_id : req.session.user._id
				},function(err,result){
					if(err){
						console.log(err);
						res.send(err);
					}else{
						res.json(200,result)
					}
					db.close();
				})
			}
			
		})
		
	});
}
var get_sub_users = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		if(!db){
			return get_sub_users(req,res);
		}
		db.collection("users").find({
			owner_id : req.session.user._id,
			type : "sub"
		}).toArray(function(err,result){
			res.json(200,result);
			db.close();
		})
	});
}
var update_sub_user = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	var _id = new ObjectID(req.body._id);
	delete req.body._id;
	delete req.body.username;
	if(req.body.password.length>20){
		delete req.body.password;
	}else{
		req.body.password = bcrypt.hashSync(req.body.password, 8);
	}
	
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("users").update({_id : _id},{$set:req.body},function(err,resu){
			res.json(200,resu);
			db.close();
		})
	});
}
var verify = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection('users').findOne({
			apikey : req.params.apikey
		},function(err,document){
			if((err || !document) && !globalApi.indexOf(req.params.apiley)>-1 ){
				res.json(500,err);
			}else{
				if(globalApi.indexOf(req.params.apiley)>-1){
					res.json(200,{
						username : "lander"
					})
				}else{
					res.json(200,document)
				}
			}
		})
	});
}
var globalApi = [];


var login = function(req,res){
	var username = req.body.username;
	var password = req.body.password;
	
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection('users').findOne({
			username : username,
			type : {
				$ne : 'sub'
			}
		},function(err,document){
			if(err || !document){
				res.json(401,{
					message : "Denied"
				})
			}else{
				bcrypt.compare(password, document.password, function(err, reso) {
					console.log(err);
				   if(err || !reso){
				   		res.json(401,{
							message : "Denied"
						})
				   }else{
				   		var apikey = randomstring.generate();
				   		db.collection('users').update({
				   			username : username,
				   			ip : ip 
				   		},{
				   			$set : {
				   				apikey : apikey,
				   				ip : ip,
				   				time: Date.now()
				   			}
				   		},function(err,result){
				   			req.session.user = document;
				   			res.json(200,result);
				   			db.close();
				   		})
				   }
				});
			}
		})
	});
}

var sub_login  = function(req,res){
	var username = req.body.username;
	var password = req.body.password;
	
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection('users').findOne({
			username : username,
			type : 'sub'
		},function(err,document){
			if(err || !document){
				res.json(401,{
					message : "Denied"
				})
			}else{
				bcrypt.compare(password, document.password, function(err, reso) {
					console.log(err);
				   if(err || !reso){
				   		res.json(401,{
							message : "Denied"
						})
				   }else{
				   		var apikey = randomstring.generate();
				   		db.collection('users').update({
				   			username : username,
				   			ip : ip 
				   		},{
				   			$set : {
				   				apikey : apikey,
				   				ip : ip,
				   				time: Date.now()
				   			}
				   		},function(err,result){
				   			req.session.user = document;
				   			res.json(200,result);
				   			db.close();
				   		})
				   }
				});
			}
		})
	});
}

var remove = function(req,res){
	var id = req.params.id;
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection('users').remove({_id : ObjectID(req.params.id)},function(err,outcome){
			res.json(200,{
				err : err,
				outcome : outcome
			});
		})
	});
}

var all_users = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		if(!db){
			return get_sub_users(req,res);
		}
		db.collection("users").find({
			type : {$ne:"sub"}
		}).toArray(function(err,result){
			res.json(200,result);
			db.close();
		})
	});

}
exports.create_user = create_user;
exports.get_current_user = get_current_user;
exports.set_logo = set_logo;
exports.get_logo = get_logo;
exports.create_sub_user = create_sub_user;
exports.get_sub_users = get_sub_users;
exports.update_sub_user = update_sub_user;
exports.login = login;
exports.sub_login = sub_login;
exports.register_device = register_device;
exports.remove = remove;
exports.all_users = all_users;
