var acl = require('acl');
var ObjectID = require('mongodb').ObjectID;
var mongodb = require('mongodb'); 
mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
  acl = new acl.mongodbBackend(db, 'acl_');
}); 
var MongoClient = require('mongodb').MongoClient;
var create_organisation = function(req,res){
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(!db){
	    	return create_organisation(req,res);
	    }
	    var collection = db.collection('organisation');
	    req.body.owner_id = req.session.user._id;
	    collection.insert(req.body,function(err,docs){
	    	if(err){
	    		res.json(500,{
	    			"code" : 500,
	    			"message" : err
	    		})
	    	}else{
	    		if(req.body.parent){
	    			acl.addRoleParents(req.body.name,req.body.parent,function(err){
	    				
	    			})
	    		}
	    		res.json(200,docs)
	    	}
	    	db.close();
	    })
	 })
}

var add_user_to_organisation = function(req,res){
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(err) throw err;
	    var collection = db.collection('users');
	    collection.update({_id : ObjectID(req.body._id)},{
	    	$push : {
	    		roles : req.params.role
	    	}
	    },function(err){
	    	if(err){
	    		res.json(500,{
	    			"code" : 500,
	    			"message" : err
	    		})
	    		db.close();
	    	}else{
	    		acl.addUserRoles(req.body._id,req.params.role,function(err){
	    			if(err){
			    		res.json(500,{
			    			"code" : 500,
			    			"message" : err
			    		})
			    	}else{
			    		res.json(200,{
				    		"code" : 200,
				    		"message" : "OK"
				    	})
			    	}
			    	db.close();
	    		});
	    	}
	    });
	 })
}

exports.create_organisation = create_organisation;
exports.add_user_to_organisation = add_user_to_organisation;
