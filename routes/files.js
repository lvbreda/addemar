var Db = require('mongodb').Db;
var MongoClient = require('mongodb').MongoClient;
var GridStore = require('mongodb').GridStore;
var mongodb = require('mongodb'); 
var Grid = require('gridfs-stream');
var ObjectID = require('mongodb').ObjectID;
var mime = require('mime')
var database;
var async = require('async');
var zipstream = require('zipstream');


mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
  database = db;
});
var get_file = function(req,res){
	
		var gfs = Grid(database, mongodb);
		try{
			var id = ObjectID(req.params.id);
		}catch(e){
			res.json(500,e);
			return;
		}
		gfs.files.findOne({ _id : ObjectID(req.params.id) },function(err,result){
    		if(err){res.json(500,err);return;}
    		if(!result){res.json(404,{});return;}
		   	var gfs = Grid(database, mongodb);

	
			var readstream = gfs.createReadStream({
			  _id : req.params.id
			});
			res.setHeader('Content-type', mime.lookup(result.filename));
			readstream.on('error', function (err) {

			  res.json(500,err);
			 
			});
			readstream.on('close',function(err){
				
			})
			readstream.pipe(res);
    	})
    	
	
}
var get_single_info = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
    	var gfs = Grid(db, mongodb);
    	gfs.files.findOne({ _id : ObjectID(req.params.id) },function(err,result){
    		 if(err) res.json(500,err);

		   	 res.json(200,result);
		   	 db.close();
    	})
    });
}
var get_files = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
    	var gfs = Grid(db, mongodb);
    	
    		gfs.files.find({ _id : {$in : idArray} }).toArray(function (err, files) {
			   if(err) res.json(500,err);

			   res.json(200,files);
			   db.close();
			})
    	
    	
    });
}
var update_file = function(id,metadata,req,callback){
	
    	var gfs = Grid(database, mongodb);
    	gfs.files.update({ _id : id,"metadata.company" : req.session.user._id },{
    		$set : {
    			"metadata" : metadata
    		}
    	},function(err){
    		callback(err);
    	})
    
}
var admin_files = function(req,res){
	if(!req.session.user){
		res.json(401,{});
		return;
	}
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		if(!db){
			return admin_files(req,res);
		}
    	var gfs = Grid(db, mongodb);
    	gfs.files.find({"metadata.company" : req.session.user._id, "metadata.type" : {$ne:"thumb"}}).toArray(function (err, files) {
		   if(err) res.json(500,err);
		   res.json(200,files);
		   db.close();
		})
    });

	
}
var delete_file = function(req,res){
    console.log({$or:[{_id : ObjectID(req.params.id)},{"metadata.parent": ObjectID(req.params.id)}], "metadata.company" : req.session.user._id});
    	var gfs = Grid(database, mongodb);
    	gfs.files.remove({$or:[{_id : ObjectID(req.params.id)},{"metadata.parent": ObjectID(req.params.id)}], "metadata.company" : req.session.user._id},function(err,result){
    		res.json(200,{});
    	})
}


var get_zip = function(req,res){
	var zip = zipstream.createZip({ level: 1 });
	var files = req.params.files.split(',');
	console.log(files);
	var gfs = Grid(database, mongodb);
	zip.pipe(res);
	async.forEachLimit(files,1,function(item,call){
		gfs.files.findOne({ _id : ObjectID(item) },function(err,result){
    		if(result){
					var readstream = gfs.createReadStream({
					  _id : item
					});
					zip.addFile(readstream, { name: result.filename },function(){
						console.log(result.filename);
						call();
					});
				}else{
					call();
				}
		   
    	})
	},function(err){
		zip.finalize(function(w) { console.log('zip done'); });
	})
}
exports.get_file = get_file;
exports.get_files = get_files;
exports.get_single_info = get_single_info;
exports.update_file = update_file;
exports.admin_files = admin_files;
exports.delete_file = delete_file;
exports.get_zip = get_zip;