
var Db = require('mongodb').Db;
var MongoClient = require('mongodb').MongoClient;
var GridStore = require('mongodb').GridStore;
var Busboy = require('busboy');
//var db = new Db('file_host', new Server('localhost', 27017));

var acl = require('acl');
var ObjectID = require('mongodb').ObjectID;
var mongodb = require('mongodb'); 
var Grid = require('gridfs-stream');

var fileObject = require('./files.js');
var gm = require('gm');

var uploaded_files = [];
var database;
mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
  database = db;
});


var uploader = function(req, res) {
    var cntProcessingFiles = 0;
  
    var gfs = Grid(database, mongodb);
    var busboy = new Busboy({ headers: req.headers });
    busboy.on('file', function(fieldname, stream, filename, encoding, contentType) {
      
      var writestream = gfs.createWriteStream({ filename: filename,metadata:{
          company : req.session.user._id,
          parent : "none"
      }});
      stream.pipe(writestream);

      writestream.on('close', function (file) {

                    createThumb(file, function(updfile){
                        res.json(200,updfile);
                    },req.session.user._id,req);


      });
      writestream.on('error',function(err){
        console.log(err);
        res.send(500, 'duplicate');
      })
    });
   
    // form error (ie fileupload-cancel)
    busboy.on('error', function(err) {
      console.error(err);
      res.send(500, 'duplicate');
    })

    req.pipe(busboy);
  
 
 
};

var duplicate = function(md5,company,good,bad){
  var gfs = Grid(database, mongodb);
  good();
  return;
  if(uploaded_files.indexOf(md5)==-1){
    gfs.files.findOne({md5 : md5,"metadata.company":company },function(err,result){
      if(!result){
        uploaded_files.push(md5);
        good();
      }else{
        bad();
      }
    })
  }else{
    bad();
  }
  
}
var upload_link = function(req,res){
  var link = req.body.url;
  createScreenshot(link,function(outcome){
    var meta = outcome.metadata;
        meta.company = req.session.user._id;
        meta.thumb = outcome._id;
        fileObject.update_file(outcome._id, meta,req,function(){
            res.json(200,outcome);
        });

    //db.close();
  })
}


var createScreenshot = function(url,callback){
  var webshot = require('webshot');
  mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
    if(!db){
      return createScreenshot(url,callback);
    }
    var gfs = Grid(db, mongodb);
    gfs.files.findOne({ filename : url },function(err,result){
      if(result){
        callback(result);
        db.close();
        return;
      }else{
        webshot(url, function(err, renderStream) {
          console.log('Webshot error',err);
          var writestream = gfs.createWriteStream({ filename: url, metadata: {
                type:"url",
                url : url
               
          }});

          writestream.on('close', function (output) {

            callback(output);
            db.close();
          });
          writestream.on('error',function(err){
            console.log("Write", err);
          })
          renderStream.pipe(writestream);
        });
      }
    });
    
  });
  

}
var createThumb = function(file,callback,company,req){
  if((/\.(gif|jpg|jpeg|tiff|png|pdf)$/i).test(file.filename)){
   
      var gfs = Grid(database, mongodb);
      // streaming from gridfs
      var readstream = gfs.createReadStream({
        _id : file._id
      });
      readstream.on('error', function (err) {
        console.log(err);
        callback();
      });
      var writestream = gfs.createWriteStream({ filename: file.filename.split(".")[0] + "_thumb_300x" + file.filename.split(".")[1], metadata: {
            type:"thumb",
            company : company,
            parent : file._id
      }});
      writestream.on('close', function (thumb) {
        var meta = file.metadata;
        meta.thumb = thumb._id
        file.metadata = meta;
        fileObject.update_file(file._id, meta,req,function(){
            callback(file);
        });

      });
      writestream.on('error',function(err){
          console.log(err);
        callback();
      })
      gm(readstream).resize(300).stream('png').pipe(writestream)
    
  }else{
    callback(file);
    return;
  }
}
 
exports.uploader = uploader;
exports.upload_link = upload_link;
