var apn = require('apn');
var mongodb = require('mongodb'); 
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var Grid = require('gridfs-stream');
var  _ = require('underscore');

exports.send_push = function(req,res){
	var message =  req.body.message
	var userIds = req.body.user;
	console.log(message,userIds);

	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		req.body.user_id = req.session.user._id;
		db.collection("msg").insert(req.body,function(err){
			if(err){
				console.log(err);
			}else{
				
			}
		})
	});
	var options = { 
		'gateway': 'gateway.push.apple.com',
		'cert' : __dirname+'/../certificates/cert.pem',
		'key' : __dirname+'/../certificates/key.pem',
		'passphrase' : 'semicolon2-desolates'};
	console.log("Cert",__dirname+'/../certificates/cert.pem');
    var apnConnection = new apn.Connection(options);
    apnConnection.on('connected', function() {
	    console.log("Connected");
	});

	apnConnection.on('transmitted', function(notification, device) {
	    console.log("Notification transmitted to:" + device.token.toString('hex'));
	});

	apnConnection.on('transmissionError', function(errCode, notification, device) {
	    console.error("Notification caused error: " + errCode + " for device ", device, notification);
	});

	apnConnection.on('timeout', function () {
	    console.log("Connection Timeout");
	});

	apnConnection.on('disconnected', function() {
	    console.log("Disconnected from APNS");
	});

apnConnection.on('socketError', console.error);
	MongoClient.connect('mongodb://127.0.0.1:27017/file_host', function(err, db) {
	    if(!db){
	    	return send_push(req,res);
	    }
	    var collection = db.collection('users');
	    for(var i in userIds){
	    	if(!_.isEmpty(userIds[i])){
	    		try{
	    			userIds[i] = ObjectID(userIds[i]);
	    		}catch(e){

	    		}
	    	}
	    	
	    }
	    var devices = [];
	   	collection.find({_id : {$in : userIds}}).toArray(function(err,users){
	   		var pushed = [];
	   		_.each(users,function(user){
	   			_.each(user.devices,function(device){
	   				if(pushed.indexOf(device) == -1){
	   					pushed.push(device);
	   					devices.push(new apn.Device(device))
	   				}
	   			})
	   		})
	   		var note = new apn.Notification();

			note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
			note.badge = 3;
			note.sound = 'ping.aiff';
			note.alert = message ;
			note.payload = {'messageFrom': 'TabletPrezi'};

			_.each(devices,function(device){
				apnConnection.pushNotification(note, device);
			})
			res.json(200,{
				"ok" : "ok"
			})
			db.close();
	   	});
	 })
}
exports.get_push = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("msg").find({user_id : req.session.user._id}).sort({_id:-1}).limit(10).toArray(function(err,contacts){
			res.json(200,contacts);
			db.close();
		})
	});
}
exports.get_gepushed = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("msg").find({"user._id" : req.session.user._id}).toArray(function(err,contacts){
			res.json(200,contacts);
			db.close();
		})
	});
}