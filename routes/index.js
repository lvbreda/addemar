var uploader = require('./upload.js');
var files = require('./files.js');
var organisations = require('./organisations.js');
var acl = require('./acl.js');
var users = require('./users.js');
var stats = require('./stats.js');
var notifications = require('./notification.js');
var mails = require('./mail.js');
var contact = require('./contact.js');
module.exports = function (app) {
    app.get('/', function (req, res, next) {
        res.render('index', {
            title: 'File host'
        });
    });

    app.get('/current',users.get_current_user);
    app.post('/current/logo',users.set_logo);
    /**
    *	Uploading
    **/
    app.post('/upload',uploader.uploader);

    app.post('/upload/link',uploader.upload_link);
    /**
    *	Retrieving file list and downloading
    **/
    app.get('/files',files.admin_files);
    app.get('/file/:id',files.get_file);
    app.get('/file/:id/info',files.get_single_info);
    app.delete('/file/:id',files.delete_file);
    app.put('/file/:id',function(req,res){
        files.update_file(req.params.id,req.body,req,function(err){

        });
    })
    app.get('/files/:role',files.get_files);

    app.get('/zip/:files',files.get_zip);
    /**
	*	Update organisation
	**/
    app.get('/organisation/detail/:id', organisations.get_organisation_detailed);
    app.get('/organisation/:id',organisations.get_organisation);
    app.get('/organisation',organisations.get_organisations);
	app.put('/organisation/:id', organisations.update_organisation);
    app.delete('/organisation/:id',organisations.delete_organisation);
	/**
	*	Update and create ACL objects
	**/
	app.post('/organisation', acl.create_organisation);

	app.post('/organisation/:role/users',acl.add_user_to_organisation);
	/*
	*	Users create and login
	*/
	app.post('/users/create',users.create_user);
	app.post('/users/login',users.login);
    app.get('/users',users.all_users);

    app.post('/users/sub/login',users.sub_login);
    app.post('/users/sub',users.create_sub_user);
    app.put('/users/sub',users.update_sub_user);
    app.get('/users/sub',users.get_sub_users);
    app.post('/users/sub/register',users.register_device);
    app.get('/users/sub/logo',users.get_logo);
    app.delete('/users/sub/:id',users.remove);

    app.get('/users/sub/organisations',organisations.get_organisations_sub);


    app.post('/stats/mobile',stats.addExternal);
    app.get('/stats',stats.getOpened);

    app.post('/notifications/send',notifications.send_push);
    app.get('/notifications',notifications.get_push);
    app.get('/sub/notifications',notifications.get_gepushed)


    app.post('/mail/send',mails.send_mail);

    app.post('/contacts',contact.addContact);
    app.get('/contacts',contact.getContacts);
    app.delete('/contact/:id',contact.deleteContact);
    app.get('/orgcontacts',contact.getOrgContacts);
};
