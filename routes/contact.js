var mongodb = require('mongodb'); 
var MongoClient = require('mongodb').MongoClient;
var  ObjectID = require('mongodb').ObjectID;
var Grid = require('gridfs-stream');
var _ = require('underscore');

exports.addContact = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		req.body.user_id = req.session.user._id;
		db.collection("contacts").findOne({id : req.body.id},function(err,contact){
			if(!contact){
				db.collection("contacts").insert(req.body,function(err){
					if(err){
						console.log(err);
					}else{
						res.json(200,{
							msg : "Added"
						})
					}
					db.close();
				})
			}else{
				delete req.body._id;
				db.collection("contacts").update({id:req.body.id},{$set:req.body},function(err){
					if(err){
						console.log(err);
					}else{
						res.json(200,{
							msg : "updated"
						})
					}
					db.close();
				})
			}
			
		})
		
	});
}
exports.getContacts = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("contacts").find({user_id : req.session.user._id}).toArray(function(err,contacts){
			res.json(200,contacts);
			db.close();
		})
	});
}
exports.getOrgContacts = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("users").find({owner_id : req.session.user._id}).toArray(function(err,users){
			var ids = _.map(users,function(user){
				return user._id.toString();
			})
			db.collection("contacts").find({user_id:{$in : ids}}).toArray(function(err,contacts){
				res.json(200,contacts);
			})
		})
	});
}

exports.deleteContact = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("contact").remove({_id : ObjectID(req.params.id)},function(err,out){
			res.json(200,out);
		})
	});
}
