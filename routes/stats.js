var mongodb = require('mongodb'); 
var MongoClient = require('mongodb').MongoClient;
var  ObjectID = require('mongodb').ObjectID;
var Grid = require('gridfs-stream');


exports.log = function(msg,extra,full){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("stats").insert({
			"message" : msg,
			"extra" : extra,
			"full" : full
		},function(err){
			if(err){
				
				console.log(err);
			}else{
				
			}
			db.close();
		})
	});
}
exports.smallLog = function(type,fileid,email){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("stats").insert({
			fileID : fileid,
			type : type,
			email : email
		},function(err){
			
			db.close();
		})
	});
}
exports.addExternal = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		req.body.type = "mobile";
		if(req.session.user){
			req.body.user_id = req.session.user._id;
		}
		db.collection("stats").insert(req.body,function(err){
			if(err){
				console.log(err);
				res.json(500,{});
			}else{
				res.json(200,{
					'message' : 'Stat added'
				})
			}
			db.close();
		})
	});
}

exports.getOpened = function(req,res){
	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
		db.collection("stats").find({}).toArray(function(err,stats){
	   		res.json(200,stats);
	   		db.close();
	   	});
	});
}