  var soap = require('soap');
  var _ = require('underscore');
  var stats = require('./stats');
  console.log(stats);
  var url = 'https://ws-demo.addemar.com/soap/wsdl/?token=475f5a5c82dfc027335dec507cfd9383&version=1.4';
 var Db = require('mongodb').Db;
var MongoClient = require('mongodb').MongoClient;
var GridStore = require('mongodb').GridStore;
var mongodb = require('mongodb'); 
var Grid = require('gridfs-stream');
var ObjectID = require('mongodb').ObjectID;

exports.send_mail = function(req,res){
	var  emails = req.body.emails.split(',');
	var  files = req.body.files;
	var  content = req.body.content;
	var fileList = "";
	files = files.replace('(','');
	files = files.replace(')','');
	files = files.replace(/\s/g,'');
	files = files.split(',');
	console.log(req.body.files);

	mongodb.connect("mongodb://127.0.0.1:27017/file_host", function(error, db) {
    	var gfs = Grid(db, mongodb);
    	var temp = [];
    	for(var i in files){
    		try{
    			temp.push(ObjectID(files[i]));
    		}catch(e){
    			console.log(files[i]);
    		}
    		
    	}
    	gfs.files.find({ _id : {$in : temp} }).toArray(function (err, files) {
		   if(err) res.json(500,err);

		   _.each(files,function(file){
		   		if(file.metadata.type=='url'){
		   			fileList += "<li><a href='" + file.filename + "'>" + file.filename + "</a></li>";
					for(var i in emails){
						stats.smallLog('mail',file._id,emails[i])
					}
		   		}else{
		   			var createdName = file.filename.split(".");
		   			var n = file.filename;
		   			var ex = createdName[createdName.length-1];
		   			n = n.replace("." + ex,'');
		   			fileList += "<li><a href='http://app.bizzerbox.com/file/" + file._id + "'>" + n + '(' + ex + ')' + "</a></li>";
					for(var i in emails){
						stats.smallLog('mail',file._id,emails[i])
					}
		   		}
				
			})
			
		    soap.createClient(url, function(err, client){
		        if (err) {
		            console.log('Error creating SOAP client : ', err); // an error occurred
		        }
		        else {
		        	console.log(fileList);
		            var fielddata = {item : [{
		             name: 'ts_links', value: '<ul>'+ fileList +'</ul>'},{name: 'ts_user', value:req.session.user.firstname + " " + req.session.user.lastname},{name: 'ts_message',
		             value:content.replace('\n','<br/>')}]};
		             for(var i in emails){
		             	 var args = {to:emails[i], ciid: '3', content : fielddata, insertContact: 'true' };

			            client.sendTriggeredItem(args, function(err, result, raw){
			                if (err) console.log('Error calling SOAP sendTriggeredItem : ', err); // an error occurred
			                /**console.log(client.lastRequest);
			                console.log(JSON.stringify(result));**/
			                res.json({result:"ok",res : result,raw : raw});
			            });
		             }
		           
		        }
		    });
		})
    });
	
	
	
}